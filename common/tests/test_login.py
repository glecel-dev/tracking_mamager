# Create your tests here.
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from service_manager_backend.auth.serializers import SMTokenObtainPairSerializer
from staff.models import User


class TestLogIn(APITestCase):
    def setUp(self):
        self.user = User(email="test@test.com")
        self.user.set_password('test2020')
        self.user.save()
        self.token = SMTokenObtainPairSerializer.get_token(self.user).access_token

    def test_successful_login(self):
        response = self.client.post(reverse('knock-knock'), {'email': 'test@test.com', 'password': 'test2020'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_failed_login(self):
        response = self.client.post(reverse('knock-knock'), {'email': 'test@test.com', 'password': '12345789'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
