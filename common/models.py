import json
import logging

from django.conf import settings
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models import F
from django.utils import timezone

logger = logging.getLogger(__name__)


class SCModel(models.Model):
    class Meta:
        app_label = 'Common'
        abstract = True


class Counter(SCModel):
    class Meta:
        verbose_name = 'Numëruesi'
        verbose_name_plural = 'Numëruesit'
        db_table = 'sc_counter'

    code = models.CharField('Emërtimi', max_length=20, primary_key=True)
    counter = models.IntegerField('Vlera', default=1)

    @classmethod
    def next(cls, code):
        try:
            cls.objects.filter(pk=code).update(counter=F('counter') + 1)
            logger.info(cls.objects.get(pk=code))
            return cls.objects.get(pk=code).counter
        except cls.DoesNotExist:
            return cls.objects.create(code=code, counter=1).counter

    def __str__(self):
        return '{}: {}'.format(self.code, self.counter)


class ACLogEntryManager(models.Manager):
    use_in_migrations = True

    def log_action(self, user_id, user_ip, content_type_id, object_id, object_repr, action_flag, change_message=''):
        if isinstance(change_message, list):
            change_message = json.dumps(change_message)
        self.model.objects.create(
            user_id=user_id,
            user_ip=user_ip,
            content_type_id=content_type_id,
            object_id=object_id,
            object_repr=object_repr[:200],
            action_flag=action_flag,
            change_message=change_message
        )


class ACActionLogger(models.Model):
    class Meta:
        verbose_name = 'Action Logger'
        verbose_name_plural = 'Action Loggers'
        db_table = 'sc_action_logger'

    action_time = models.DateTimeField('Action time', default=timezone.now, editable=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE, verbose_name='User', null=True)
    user_ip = models.CharField('User IP', max_length=20)
    content_type = models.ForeignKey(ContentType, models.SET_NULL, verbose_name='Content Type', blank=True, null=True)
    object_id = models.TextField('Object id', blank=True, null=True)
    object_repr = models.CharField('Object repr', max_length=200)
    action_flag = models.PositiveSmallIntegerField('Action flag')
    change_message = models.TextField('Change message', blank=True)
    objects = ACLogEntryManager()


class Configuration(models.Model):
    class Meta:
        verbose_name = 'Configuration'
        verbose_name_plural = 'Configurations'
        db_table = 'sc_configuration'

    name = models.CharField('Emërtimi', max_length=50)
    value = models.CharField('Vlera', max_length=50)

    def __str__(self):
        return self.name
