from django.contrib import admin

# Register your models here.
from common.models import Configuration

admin.site.register(Configuration)
