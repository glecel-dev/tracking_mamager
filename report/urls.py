from django.urls import path

from report.views.report_views import *

urlpatterns = [
    # path('report/process-time/service-item/', get_avg_process_time_of_service_item, name='process-time'),
    path('report/average-time-per-agent/', request_average_time_per_agent, name='average-time-per-agent'),
    path('report/sales-per-agent/', request_sales_per_agent, name='sales-per-agent'),
    path('report/sales-per-month/', request_sales_per_month, name='sales-per-month'),
    path('report/service-items-per-month/', request_service_items_per_month, name='service-items-per-month'),
    path('report/total-price-per-month/', request_total_price_per_month, name='total-price-per-month'),
    path('report/success-report/', request_success_report, name='success-report'),
]
