AVERAGE_TIME_PER_AGENT = """
select agentid, first_name, last_name, avg(duration), avg(hours) from(
select agentid, first_name, last_name,
       state_change2.date_created - in_reparir_state_date as duration,
       extract(epoch from state_change2.date_created - in_reparir_state_date)/3600 as hours
from (select agent.id                  as agentid,
                agent.first_name        as first_name,
                agent.last_name         as last_name,
             service_item_id           as service_itemid,
             state_change.date_created as in_reparir_state_date
      from auth_user agent
               inner join sc_service_state_change state_change on agent.id = state_change.agent_id
               inner join sc_service_item service_item on state_change.service_item_id = service_item.id
      where (start_state = %s and end_state = %s) and state_change.date_created::date >= %s::date) as inrepair
         inner join sc_service_state_change state_change2 on
             state_change2.service_item_id = service_itemid and state_change2.agent_id = agentid
where (start_state = %s and end_state = %s) and state_change2.date_created::date<=%s::date) as duration_query
group by agentid, first_name, last_name;
"""

SALES_PER_AGENT = """
select
       auth_user.id as agent_id,
        auth_user.first_name as first_name, 
        auth_user.last_name as last_name, 
        count(*) as total_sales
from  auth_user
         inner join sc_warranty sw on auth_user.id = sw.creator_id
         inner join sc_warranty_unit swu on sw.id = swu.warranty_id
where sw.date_created::date between %s::date and %s::date
group by auth_user.id, auth_user.first_name, auth_user.last_name
"""

SALES_PER_MONTH = """
select EXTRACT(month from sw.date_created)::int as month, count(*) as total_sales
from sc_warranty sw
inner join sc_warranty_unit swu on sw.id = swu.warranty_id
where extract(year from sw.date_created)=%s
group by EXTRACT(month from sw.date_created);
"""

SERVICE_ITEMS_REGISTERED_PER_MONTH = """
select EXTRACT(month from ssi.date_created)::int as month, count(*)
from sc_service_item ssi
where extract(year from ssi.date_created)=%s
group by EXTRACT(month from ssi.date_created);
"""

SERVICE_ITEMS_DELIVERED_PER_MONTH = """
select EXTRACT(month from ssi.date_created)::int as month, count(*)
from sc_service_item ssi
         inner join sc_service_state_change sssc on ssi.id = sssc.service_item_id
where extract(year from ssi.date_created) = %s
  and sssc.end_state = %s
group by EXTRACT(month from ssi.date_created);
"""

TOTAL_PRICE_PER_MONTH = """
select EXTRACT(month from ssi.clearance_date)::int as month,
       sum(price)
from sc_service_item ssi
         inner join sc_service_state_change sssc on ssi.id = sssc.service_item_id
         inner join sc_service_item_service_type ssist on ssi.id = ssist.service_item_id
where extract(year from ssi.clearance_date) = %s
  and sssc.end_state = %s
group by EXTRACT(month from ssi.clearance_date);
"""

SUCCESS_REPORT = """
select count(sc_service_item.id)                    as total,
       count(sc_service_item.id) - count(parent_id) as successful,
       count(parent_id)                             as failed
from sc_service_item
         inner join sc_service_state_change sssc
                    on sc_service_item.id = sssc.service_item_id
where sssc.end_state = %s and sssc.date_created::date between %s::date and %s::date
"""
