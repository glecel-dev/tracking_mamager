from datetime import date
from datetime import timedelta

from django.db.models import Q, F, ExpressionWrapper, DurationField, Min, Max
from django.http import JsonResponse
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from report.views.report_query_functions import *
from service.cons import *
from service.models import ServiceItem, ServiceItemStateChange
from service_manager_backend.common.api_permissions import IsAdmin
from service_manager_backend.cons import DATA
from service_manager_backend.settings import POSTGRES_DATE_FORMAT
from staff.models import User


@api_view(['POST'])
@permission_classes([IsAdmin])
def request_average_time_per_agent(request):
    data = request.data
    date_created_range = ServiceItemStateChange.objects.aggregate(Min('date_created'), Max('date_created'))
    date_min = date_created_range['date_created__min'].strftime(POSTGRES_DATE_FORMAT) if date_created_range[
        'date_created__min'] else None
    date_max = date_created_range['date_created__max'].strftime(POSTGRES_DATE_FORMAT) if date_created_range[
        'date_created__max'] else None
    start_date = data['start_date'] if data['start_date'] else date_min
    end_date = data['end_date'] if data['end_date'] else date_max
    query_result = get_average_time_per_agent([ACCEPTED, IN_REPAIR, start_date, IN_REPAIR, NOTIFIED_CLIENT, end_date])
    agent_name_list, agent_hours_list = [], []
    for result in query_result['agent_list']:
        agent_name_list.append('{} {}'.format(result['first_name'], result['last_name']))
        agent_hours_list.append(round(result['hours'], 4))
    return JsonResponse({DATA: {'total_avg_hours': round(query_result['overall_average'], 2),
                                'agent_avg_time': {'label_name': agent_name_list, 'data_hours': agent_hours_list}}})


@api_view(['POST'])
@permission_classes([AllowAny])
def request_sales_per_agent(request):
    data = request.data
    date_created_range = ServiceItemStateChange.objects.aggregate(Min('date_created'), Max('date_created'))
    date_min = date_created_range['date_created__min'].strftime(POSTGRES_DATE_FORMAT) if date_created_range[
        'date_created__min'] else None
    date_max = date_created_range['date_created__max'].strftime(POSTGRES_DATE_FORMAT) if date_created_range[
        'date_created__max'] else None
    start_date = data['start_date'] if data['start_date'] else date_min
    end_date = data['end_date'] if data['end_date'] else date_max
    query_result = get_sales_per_agent([start_date, end_date])

    agent_name_list, agent_total_sales = [], []
    for result in query_result['agent_list']:
        agent_name_list.append(result['first_name'] + ' ' + result['last_name'])
        agent_total_sales.append(result['total_sales'])
    return JsonResponse({DATA: {'overall_sales': query_result['overall_sales'],
                                'agents': {'agent_name': agent_name_list, 'agent_total_sales': agent_total_sales}}})


@api_view(['POST'])
@permission_classes([IsAdmin])
def request_sales_per_month(request):
    data = request.data
    year = data['year']
    query_result = get_sales_per_month([year])
    month_total_sales = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for result in query_result:
        month_total_sales[result['month'] - 1] = result['total_sales']
    return JsonResponse(
        {DATA: {'month_total_sales': month_total_sales,
                'year': data['year']}})


@api_view(['POST'])
@permission_classes([IsAdmin])
def request_service_items_per_month(request):
    data = request.data
    year = data['year']
    query_result_registered = get_service_items_registered_per_month([year])
    query_result_delivered = get_service_items_delivered_per_month([year, DELIVERED])
    result_registered = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    result_delivered = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for registered, delivered in zip(query_result_registered, query_result_delivered):
        result_registered[registered['month'] - 1] = registered['total_sales']
        result_delivered[delivered['month'] - 1] = delivered['total_sales']
    return JsonResponse({DATA: {'result_registered': result_registered, 'result_delivered': result_delivered}})


@api_view(['POST'])
@permission_classes([IsAdmin])
def request_total_price_per_month(request):
    data = request.data
    year = data['year']
    query_result = get_total_price_per_month([year, DELIVERED])
    total_price_per_month = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for result in query_result:
        total_price_per_month[result['month'] - 1] = result['total_price']
    return JsonResponse({DATA: {'total_price_per_month': total_price_per_month, 'year': data['year']}})


@api_view(['POST'])
@permission_classes([IsAdmin])
def request_success_report(request):
    data = request.data
    date_created_range = ServiceItemStateChange.objects.aggregate(Min('date_created'), Max('date_created'))
    date_min = date_created_range['date_created__min'].strftime(POSTGRES_DATE_FORMAT) if date_created_range[
        'date_created__min'] else None
    date_max = date_created_range['date_created__max'].strftime(POSTGRES_DATE_FORMAT) if date_created_range[
        'date_created__max'] else None
    start_date = data['start_date'] if data['start_date'] else date_min
    end_date = data['end_date'] if data['end_date'] else date_max
    query_result = get_success_report([DELIVERED, start_date, end_date])
    return JsonResponse({DATA: query_result[0]})
