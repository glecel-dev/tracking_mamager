from django.db import connection

from report.views.report_queries import *


def get_query_results(query, params):
    try:
        with connection.cursor() as cursor:
            cursor.execute(query, params)
            rows = cursor.fetchall()
        return rows
    except Exception as e:
        return None


def generate_duration_label(duration):
    label = ''
    if duration.days:
        label += '{} days'.format(duration.days)
    hours = duration.seconds // 3600
    if hours:
        label += '{} hours'.format(hours)
    minutes = duration.seconds // 60 % 60
    if minutes:
        label += '{} minutes'.format(minutes)
    label += '{} seconds'.format(round(duration.seconds, 0))
    return label


def get_average_time_per_agent(data_list):
    query_results = get_query_results(AVERAGE_TIME_PER_AGENT, data_list)
    result = {'agent_list': [], 'overall_average': 0}
    sum = 0
    if query_results:
        for row in query_results:
            row_data = {
                'agent_id': row[0],
                'first_name': row[1],
                'last_name': row[2],
                'duration': row[3],
                'duration_label': generate_duration_label(row[3]),
                'hours': row[4],
            }
            sum += row[4]
            result['agent_list'].append(row_data)
        result['overall_average'] = sum / len(query_results)
    return result


def get_sales_per_agent(data_list):
    query_results = get_query_results(SALES_PER_AGENT, data_list)
    result = {'agent_list': [], 'overall_sales': 0}
    sum = 0
    if query_results:
        for row in query_results:
            row_data = {
                'agent_id': row[0],
                'first_name': row[1],
                'last_name': row[2],
                'total_sales': row[3],
            }
            sum += row[3]
            result['agent_list'].append(row_data)
        result['overall_sales'] = sum
    return result


def get_sales_per_month(data_list):
    query_results = get_query_results(SALES_PER_MONTH, data_list)
    result = []
    if query_results:
        for row in query_results:
            row_data = {
                'month': int(row[0]),
                'total_sales': row[1],
            }
            result.append(row_data)
    return result


def get_service_items_registered_per_month(data_list):
    query_results = get_query_results(SERVICE_ITEMS_REGISTERED_PER_MONTH, data_list)
    result = []
    if query_results:
        for row in query_results:
            row_data = {
                'month': row[0],
                'total_sales': row[1],
            }
            result.append(row_data)
    return result


def get_service_items_delivered_per_month(data_list):
    query_results = get_query_results(SERVICE_ITEMS_DELIVERED_PER_MONTH, data_list)
    result = []
    if query_results:
        for row in query_results:
            row_data = {
                'month': row[0],
                'total_sales': row[1],
            }
            result.append(row_data)
    return result


def get_total_price_per_month(data_list):
    query_results = get_query_results(TOTAL_PRICE_PER_MONTH, data_list)
    result = []
    if query_results:
        for row in query_results:
            row_data = {
                'month': row[0],
                'total_price': row[1],
            }
            result.append(row_data)
    return result


def get_success_report(data_list):
    query_results = get_query_results(SUCCESS_REPORT, data_list)
    result = []
    if query_results:
        for row in query_results:
            row_data = {
                'total': row[0],
                'successful': row[1],
                'failed': row[2],
            }
            result.append(row_data)
    return result
