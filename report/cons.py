from datetime import date

JANUARY = 1
FEBRUARY = 2
MARS = 3
APRIL = 4
MAY = 5
JUNE = 6
JULY = 7
AUGUST = 8
SEPTEMBER = 9
OCTOBER = 10
NOVEMBER = 11
DECEMBER = 12

MONTHS_LIST = {
    JANUARY: 'Janar',
    FEBRUARY: 'Shkurt',
    MARS: 'Mars',
    APRIL: 'Prill',
    MAY: 'Maj',
    JUNE: 'Qershor',
    JULY: 'Korrik',
    AUGUST: 'Gusht',
    SEPTEMBER: 'Shtator',
    OCTOBER: 'Tetor',
    NOVEMBER: 'Nëntor',
    DECEMBER: 'Dhjetor',
}

year = date.today().year
years_list = [str(year - i) for i in range(10)]