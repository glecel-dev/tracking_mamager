from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.http import JsonResponse
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from service_manager_backend.common.api_permissions import IsAdmin
from service_manager_backend.common.api_views import SMListAPIView
from service_manager_backend.cons import DATA
from staff.cons import STATE_CHANGE_PERMISSIONS
from staff.model_serializers.group_serializers import GroupReadSerializer
from staff.model_serializers.permissions_serializers import PermissionReadSerializer


class PermissionListAPIView(SMListAPIView):
    queryset = Group.objects.all()
    serializer_class = PermissionReadSerializer


@api_view(['GET'])
@permission_classes([AllowAny])
def get_permissions_table(request):
    models = [
        'customer',
        'agent',
        'warranty',
        'serviceitem',
        'servicetype',
        'reminder',
    ]
    permissions_table = []
    content_types = ContentType.objects.filter(model__in=models)
    for content_type in content_types:
        permissions = content_type.permission_set.all()
        unit_data = {
            'name': content_type.name,
            'view_perm': permissions.filter(codename__startswith='view_').first().id,
            'add_perm': permissions.filter(codename__startswith='add_').first().id,
            'change_perm': permissions.filter(codename__startswith='change_').first().id,
            'delete_perm': permissions.filter(codename__startswith='delete_').first().id,
        }
        permissions_table.append(unit_data)

    service_item_content_type = ContentType.objects.get(model='serviceitem')
    change_state_perms = service_item_content_type.permission_set.exclude(codename__in=['add_serviceitem',
                                                                                        'view_serviceitem',
                                                                                        'change_serviceitem',
                                                                                        'delete_serviceitem'])
    change_state_perms_table = []
    for perm in change_state_perms:
        change_state_perms_table.append({
            'name': STATE_CHANGE_PERMISSIONS[perm.codename] if perm.codename in STATE_CHANGE_PERMISSIONS else perm.name,
            'id': perm.id
        })
    groups = Group.objects.all()
    if not request.user.is_superuser:
        groups = groups.exclude(name='Administrator')
    return Response({DATA: {'permissions_table': permissions_table,
                            'change_state_permissions_table': change_state_perms_table,
                            'groups': list(
                                GroupReadSerializer(groups.order_by('name'), many=True).data)}})


@api_view(['GET'])
@permission_classes([AllowAny])
def get_group_permissions(request, group_id):
    return Response({DATA: list(Group.objects.get(id=group_id).permissions.values_list('id', flat=True))})


@api_view(['POST'])
@permission_classes([AllowAny])
def add_permission(request):
    data = request.data
    if 'permission_id' in data and 'group_id' in data:
        permission_id = int(data['permission_id'])
        group_id = int(data['group_id'])
        group = Group.objects.get(id=group_id)
        permission = Permission.objects.get(id=permission_id)
        group.permissions.add(permission)
        return Response({}, status=200)
    else:
        return Response({}, status=500)


@api_view(['POST'])
@permission_classes([AllowAny])
def remove_permission(request):
    data = request.data
    if 'permission_id' in data and 'group_id' in data:
        permission_id = int(data['permission_id'])
        group_id = int(data['group_id'])
        group = Group.objects.get(id=group_id)
        permission = Permission.objects.get(id=permission_id)
        group.permissions.remove(permission)
        return JsonResponse({}, status=200)
    else:
        return JsonResponse({}, status=500)
