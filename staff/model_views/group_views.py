from django.contrib.auth.models import Group

from service_manager_backend.common.api_views import SMListCreateAPIView, SMRetrieveUpdateDestroyAPIView
from staff.model_serializers.group_serializers import GroupFilterSerializer, GroupReadSerializer, GroupWriteSerializer


class GroupListCreateAPIView(SMListCreateAPIView):
    queryset = Group.objects.all().order_by('name')
    read_serializer_class = GroupReadSerializer
    write_serializer_class = GroupWriteSerializer
    filter_serializer_class = GroupFilterSerializer
    filter_map = {}


class GroupRetrieveUpdateDestroyAPIView(SMRetrieveUpdateDestroyAPIView):
    queryset = Group.objects.all().order_by('name')
    read_serializer_class = GroupReadSerializer
    write_serializer_class = GroupWriteSerializer
