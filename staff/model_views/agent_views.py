from django.contrib.auth.models import Group
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from service_manager_backend.common.api_views import SMListCreateAPIView, SMRetrieveUpdateDestroyAPIView
from service_manager_backend.cons import DATA, MESSAGE
from staff.model_serializers.agent_serializers import AgentFilterSerializer, AgentReadSerializer, AgentWriteSerializer
from staff.models import User


class AgentListCreateAPIView(SMListCreateAPIView):
    queryset = User.objects.filter(is_active=True).exclude(is_superuser=True)
    read_serializer_class = AgentReadSerializer
    write_serializer_class = AgentWriteSerializer
    filter_serializer_class = AgentFilterSerializer
    filter_map = {}


class AgentRetrieveUpdateDestroyAPIView(SMRetrieveUpdateDestroyAPIView):
    queryset = User.objects.filter(is_active=True).exclude(is_superuser=True)
    read_serializer_class = AgentReadSerializer
    write_serializer_class = AgentWriteSerializer

    def delete(self, request, *args, **kwargs):
        user = self.get_object()
        user.is_active = False
        user.save()
        return Response({MESSAGE: 'Operacioni perfundoi me sukses'}, status=status.HTTP_204_NO_CONTENT)

# todo -  hiqe kur ta besh nga fronti
@api_view(['GET'])
def get_groups(request):
    queryset = Group.objects.all().order_by('-name')
    return Response(
        {DATA: [
            {'id': item.id,
             'name': item.name} for item in queryset]})
