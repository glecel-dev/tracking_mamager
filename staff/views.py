# Create your views here.
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from service_manager_backend.cons import DATA


@api_view(['GET'])
def get_user_info(request):
    user = request.user
    groups = list(user.groups.all().values_list('id', flat=True))
    result = {
        'groups': groups,
        'active': user.is_active,
        'firstName': user.first_name,
        'lastName': user.last_name,
        'email': user.email,
        'phone': user.phone
    }
    return Response({DATA: result}, status=status.HTTP_200_OK)


@api_view(['GET'])
def get_user_permissions(request):
    user = request.user
    groups = list(user.groups.all().values_list('name', flat=True))
    if user.is_superuser:
        groups.append('SUPERUSER')
    result = {
        'permissions': list(user.get_all_permissions()),
        'groups': groups
    }
    return Response({DATA: result}, status=status.HTTP_200_OK)
