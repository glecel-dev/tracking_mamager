from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils.translation import ugettext_lazy as _


class SCModel(models.Model):
    class Meta:
        app_label = 'staff'
        abstract = True


class MyUserManager(BaseUserManager):
    """
    A custom user manager to deal with emails as unique identifiers for auth
    instead of usernames. The default that's used is "UserManager"
    """

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given username and password.
        """
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self._create_user(email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    class Meta:
        db_table = 'auth_user'
        ordering = ['first_name', 'last_name']

    email = models.EmailField(unique=True)
    first_name = models.CharField(_('first name'), max_length=255, blank=True)
    last_name = models.CharField(_('last name'), max_length=255, blank=True)
    phone = models.CharField(_('phone'), max_length=30, blank=True)
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    USERNAME_FIELD = 'email'
    objects = MyUserManager()

    def set_password(self, raw_password):
        self.password = make_password(raw_password)
        self._password = raw_password

    def __str__(self):
        return self.email

    def get_full_name(self):
        return self.first_name + " " + self.last_name


# class Agent(SCModel):
#     class Meta:
#         verbose_name = 'Agjent'
#         verbose_name_plural = 'Agjentë'
#         db_table = 'sc_agent'
#         ordering = ['user__username']
#
#     phone = models.CharField('Telefon', max_length=50)
#     user = models.OneToOneField(User, verbose_name='User', related_name='agent', on_delete=models.CASCADE)
#     date_created = models.DateField('Data e krijimit', auto_now_add=True)
#     date_last_updated = models.DateField('Data e përditësimit të fundit', auto_now=True)
#     deleted = models.BooleanField(default=False)
#
#     def get_fullname(self):
#         return '{} {}'.format(self.user.first_name, self.user.last_name)
#
#     def __str__(self):
#         return '{}'.format(self.user.username)


class Shop(SCModel):
    class Meta:
        verbose_name = 'Shop'
        verbose_name_plural = 'Shops'
        db_table = 'sc_shop'
        ordering = ['name']

    name = models.CharField('Emri', max_length=225)
    prefix_code = models.CharField('Kodi', max_length=4)
    address = models.CharField('Adresa', max_length=225)
    contact_number = models.CharField('Numri i kontaktit', max_length=20)
    members = models.ManyToManyField(User, verbose_name='Anëtarë', related_name='shops')

    def __str__(self):
        return self.name
