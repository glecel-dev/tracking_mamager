from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
# Register your models here.
from django.contrib.auth.admin import UserAdmin

from staff.models import User, Shop


class EmailUserAdmin(UserAdmin):
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
         ),
    )
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'phone')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login',)}),
        (_('Groups'), {'fields': ('groups',)}),
    )
    list_display = ('email', 'first_name', 'last_name', 'is_staff')
    ordering = ('email',)


if admin.site.is_registered(User):
    admin.site.unregister(User)

admin.site.register(User, EmailUserAdmin)
admin.site.register(Shop)
