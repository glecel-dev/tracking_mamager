from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from staff.tests.test_utils import create_admin_user, TEST_PASSWORD, TEST_EMAIL


class TestUserAuthenticated(APITestCase):
    def setUp(self):
        self.user = create_admin_user()

    def test_successful_login(self):
        response = self.client.post(reverse('knock-knock'), {'email': TEST_EMAIL, 'password': TEST_PASSWORD}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_failed_login(self):
        response = self.client.post(reverse('knock-knock'), {'email': TEST_EMAIL, 'password': '12345789'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
