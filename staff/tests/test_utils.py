from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType

from staff.cons import GROUP_ADMINISTRATOR
from staff.models import User
from service_manager_backend.auth.serializers import SMTokenObtainPairSerializer

TEST_EMAIL = 'test@test.com'
TEST_PASSWORD = 'test2020'
TEST_PHONE = '+3556969699'


def create_admin_user():
    user = User(email=TEST_EMAIL, is_superuser=True)
    user.set_password(TEST_PASSWORD)
    user.save()
    group, created = Group.objects.get_or_create(name=GROUP_ADMINISTRATOR)
    group.user_set.add(user)
    user.save()
    generate_state_change_permissions()
    return user


def generate_user_token(user=None):
    if user is None:
        return SMTokenObtainPairSerializer.get_token(create_admin_user()).access_token
    return SMTokenObtainPairSerializer.get_token(user).access_token


def generate_state_change_permissions():
    content_type = ContentType.objects.get(app_label='service', model='serviceitem')
    perms_list = [{
        "name": "Can change state from accepted to in repair",
        "content_type": content_type,
        "codename": "accepted_in_repair"
    },
        {
            "name": "Can change state from in repair to notified_client",
            "content_type": content_type,
            "codename": "in_repair_notified_client"
        },
        {
            "name": "Can change state from in repair to rejected",
            "content_type": content_type,
            "codename": "in_repair_rejected"
        },
        {
            "name": "Can change state from notified client to delivered",
            "content_type": content_type,
            "codename": "notified_client_delivered"
        },
        {
            "name": "Can change state from notified client to in repair",
            "content_type": content_type,
            "codename": "notified_client_in_repair"
        },
        {
            "name": "Can change state from delivered to in repair",
            "content_type": content_type,
            "codename": "delivered_in_repair"
        },
        {
            "name": "Can change state from rejected to in repair",
            "content_type": content_type,
            "codename": "rejected_in_repair"
        },
        {
            "name": "Can change state from rejected to delivered",
            "content_type": content_type,
            "codename": "rejected_delivered"
        }]
    for unit in perms_list:
        new_perm = Permission(**unit)
        new_perm.save()
    return perms_list
