from django.urls import reverse
from django.test import TestCase
from model_bakery import baker
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from staff.model_serializers.agent_serializers import AgentReadSerializer, AgentWriteSerializer
from staff.models import User
from staff.tests.test_utils import generate_user_token

AGENTS_URL = reverse('agents')


def agent_url(agent_id):
    """Return specific agent URL"""
    return reverse('agent', args=[agent_id])


class PublicAgentApiTest(TestCase):
    """Test the public available Agent API"""

    def setUp(self):
        self.client = APIClient()
        self.b = baker

    def test_login_required(self):
        """Test that login is required for retrieving/creating Agent"""

        # Test required login for retrieving Agent
        res = self.client.get(AGENTS_URL)
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)

        # Test required login for creating Agent
        res = self.client.post(AGENTS_URL, {'data': 'data'}, format='json')
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)

        # Test required login for updating Agent
        user = self.b.make(User)
        url = agent_url(user.id)
        data = AgentWriteSerializer(user).data
        res = self.client.put(url, data, format='json')
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)

        # Test login required for deleting Agent
        res = self.client.delete(url)
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)


class PrivateAgentApiTest(APITestCase):
    """Test the authenticated requests"""

    def setUp(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer {}'.format(generate_user_token()))
        self.b = baker

    def test_agent_list(self):
        """
        Client requests the agent list
        :return: status code 200 ok!
        """
        user1, user2 = self.b.make(User), self.b.make(User)

        agents = User.objects.filter(is_active=True, is_superuser=False).order_by('id')
        serializer = AgentReadSerializer(agents, many=True)

        res = self.client.get(AGENTS_URL)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

        # Should be 2 Agents created (without superuser)
        self.assertEqual(len(res.data['data']), 2)
        self.assertEqual(res.data['data'], serializer.data)

    def test_agent_create(self):
        """
        Client requests to create the agent!
        :return: status code 201 CREATED!
        """

        agent_data = {
            'phone': '+4444444444',
            'first_name': 'Test Name',
            'last_name': 'Test Last name',
            'password': 'testPassword',
            'email': 'test@test.com',
            'groups': []
            }
        res = self.client.post(AGENTS_URL, agent_data, format='json')
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        self.assertEqual(res.data['data']['phone'], agent_data['phone'])
        self.assertEqual(res.data['data']['first_name'], agent_data['first_name'])
        self.assertEqual(res.data['data']['last_name'], agent_data['last_name'])
        self.assertEqual(res.data['data']['email'], agent_data['email'])

    def test_agent_update(self):
        """Test agent update"""
        user = self.b.make(User)
        agent_data = {
            'phone': '+4444444444',
            'first_name': 'Test Name',
            'last_name': 'Test Last name',
            'password': 'testPassword',
            'email': 'test@test.com',
            'groups': []
            }
        url = agent_url(user.id)

        res = self.client.put(url, agent_data, format='json')
        self.assertEqual(res.status_code, status.HTTP_200_OK)

        user.refresh_from_db()
        serializer = AgentWriteSerializer(user)
        self.assertEqual(res.data['data'], serializer.data)
        self.assertEqual(user.phone, agent_data['phone'])

    def test_agent_delete(self):
        """Test agent delete"""
        user = self.b.make(User)
        url = agent_url(user.id)

        res = self.client.delete(url)
        self.assertEqual(res.status_code, status.HTTP_204_NO_CONTENT)
        deleted_agent = User.objects.get(id=user.id)
        self.assertEqual(deleted_agent.is_active, False)


