from django.contrib.auth.models import Group
from django.test import TestCase
from django.urls import reverse
from model_bakery import baker
from rest_framework import status
from rest_framework.test import APIClient

from staff.model_serializers.group_serializers import GroupReadSerializer, GroupWriteSerializer
from staff.models import User

GROUPS_URL = reverse('groups')


def group_url(group_id):
    """Return specific group URL"""
    return reverse('group', args=[group_id])


class PublicGroupApiTest(TestCase):
    """Test the public available Group API"""

    def setUp(self):
        self.client = APIClient()

    def test_login_required(self):
        """Test that login is required for retrieving/creating Groups"""

        # Test that login is required for retrieving Groups
        res = self.client.get(GROUPS_URL)
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)

        # Test that login is required for creating Group
        res = self.client.post(GROUPS_URL, {'data': 'data'}, format='json')
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)

        # Test that login is required for updating Group
        group = Group.objects.create(name='TestGroup1')
        data = {
            'id': group.id,
            'name': 'Updated Name',
            'user_set': []
            }
        url = group_url(group.id)

        res = self.client.put(url, data, format='json')
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)

        # Test that login is required for deleting Group
        res = self.client.delete(url)
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)


class PrivateGroupApiTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(
            email='TestEmail@test.com',
            password='TestPassword',
            is_superuser=True,
            )

        self.client = APIClient()
        self.client.force_authenticate(self.user)

    def test_group_list(self):
        """
        Client requests to get the group!
        :return: status code 200 ok!
        """

        Group.objects.create(name='TestGroup1')
        Group.objects.create(name='TestGroup2')
        res = self.client.get(GROUPS_URL)

        groups = Group.objects.all()
        serializer = GroupReadSerializer(groups, many=True)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(len(res.data['data']), 2)
        self.assertEqual(res.data['data'], serializer.data)

    def test_group_create(self):
        """
        Client requests to create the group!
        :return: status code 201 CREATED!
        """

        group_data = {
            'name': 'TestName',
            'user_set': []
            }
        res = self.client.post(GROUPS_URL, group_data, format='json')

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        self.assertEqual(res.data['data']['name'], group_data['name'])

    def test_group_update(self):
        """Test group update"""
        group = Group.objects.create(name='TestGroup1')
        data = {
            'id': group.id,
            'name': 'Updated Name',
            'user_set': []
            }
        url = group_url(group.id)

        res = self.client.put(url, data, format='json')
        self.assertEqual(res.status_code, status.HTTP_200_OK)

        group.refresh_from_db()
        serializer = GroupWriteSerializer(group)
        self.assertEqual(res.data['data'], serializer.data)

    def test_group_delete(self):
        """Test group delete"""
        group = Group.objects.create(name='TestGroup1')
        url = group_url(group.id)

        res = self.client.delete(url)
        self.assertEqual(res.status_code, status.HTTP_204_NO_CONTENT)
        deleted_group = Group.objects.filter(id=group.id).exists()
        self.assertEqual(deleted_group, False)
