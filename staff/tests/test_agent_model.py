from django.test import TestCase
from faker import Faker
from model_bakery import baker

from staff.models import User, Shop


class TestAgent(TestCase):
    def setUp(self):
        self.agent = baker.make(User)
        self.fake = Faker()

    def test_agent_create(self):
        self.assertTrue(isinstance(self.agent, User))
        self.agent.save()
        # self.assertNotEqual(self.agent.phone, None)
        self.assertTrue(self.agent.id is not None)
        self.assertEqual(self.agent, User.objects.get(pk=self.agent.id))

    def test_agent_update(self):
        old_name = self.agent
        self.agent = baker.make(User)
        self.agent.save()
        self.assertNotEqual(old_name, self.agent)

    def test_agent_delete(self):
        self.agent.delete()
        self.assertFalse(User.objects.filter(pk=self.agent.id))


class TestShop(TestCase):
    def setUp(self):
        self.shop = baker.make(Shop)
        self.fake = Faker()

    def test_shop_create(self):
        self.assertTrue(isinstance(self.shop, Shop))
        self.shop.save()
        self.assertNotEqual(self.shop.name, None)
        self.assertTrue(self.shop.id is not None)
        self.assertEqual(self.shop, Shop.objects.get(pk=self.shop.id))

    def test_shop_update(self):
        old_name = self.shop.name
        self.shop.name = self.fake.name()
        self.shop.save()
        self.assertNotEqual(old_name, self.shop.name)

    def test_shop_delete(self):
        self.shop.delete()
        self.assertFalse(Shop.objects.filter(pk=self.shop.id).exists())
