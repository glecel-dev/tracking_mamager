GROUP_ADMINISTRATOR = 'Administrator'

STATE_CHANGE_PERMISSIONS = {
    'accepted_in_repair': 'Ndrysho gjendje nga Pranuar në Proces Riparimi',
    'in_repair_notified_client': 'Ndrysho gjendje nga Proces Riparimi në Njofto Klientin',
    'in_repair_rejected': 'Ndrysho gjendje nga Proces Riparimi në Refuzuar',
    'notified_client_delivered': 'Ndrysho gjendje nga Njofto Klientin në Proces Riparimi',
    'notified_client_in_repair': 'Ndrysho gjendje nga Njofto Klientin në Dorëzuar',
    'delivered_in_repair': 'Ndrysho gjendje nga Dorëzuar në Proces Riparimi',
    'rejected_in_repair': 'Ndrysho gjendje nga Refuzuar në Proces Riparimi',
    'rejected_delivered': 'Ndrysho gjendje nga Refuzuar në Dorëzuar',

}
