from django.contrib.auth.models import Group
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer


class PermissionReadSerializer(ModelSerializer):
    codename = serializers.SerializerMethodField()

    class Meta:
        model = Group
        fields = ['id', 'name', 'permissions', 'codename']

    def get_codename(self, obj):
        return list(obj.permissions.values_list('codename', flat=True))
