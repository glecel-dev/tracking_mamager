from rest_framework import serializers
from rest_framework.fields import CharField, SerializerMethodField
from rest_framework.serializers import ModelSerializer, Serializer

from staff.models import User


class AgentReadSerializer(ModelSerializer):
    full_name = SerializerMethodField()

    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'email', 'phone', 'groups', 'full_name']

    def get_full_name(self, obj):
        return obj.get_full_name()


class AgentWriteSerializer(ModelSerializer):
    password = serializers.CharField(write_only=True, required=False)

    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'email', 'phone', 'groups', 'password']

    def create(self, validated_data):
        user = super(AgentWriteSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user


class AgentFilterSerializer(Serializer):
    first_name = CharField(max_length=50, required=False, allow_blank=True, allow_null=True)
    last_name = CharField(max_length=50, required=False, allow_blank=True, allow_null=True)
    email = CharField(max_length=50, required=False, allow_blank=True, allow_null=True)
