from django.contrib.auth.models import Group
from rest_framework.fields import CharField, SerializerMethodField
from rest_framework.serializers import ModelSerializer, Serializer


class GroupReadSerializer(ModelSerializer):
    user_names = SerializerMethodField()

    class Meta:
        model = Group
        fields = ['id', 'name', 'user_set', 'user_names']

    def get_user_names(self, obj):
        return ', '.join(['{} {}'.format(user.first_name, user.last_name) for user in obj.user_set.filter(is_active=True)])


class GroupWriteSerializer(ModelSerializer):
    class Meta:
        model = Group
        fields = ['id', 'name', 'user_set']


class GroupFilterSerializer(Serializer):
    name = CharField(max_length=50, required=False, allow_blank=True, allow_null=True)
