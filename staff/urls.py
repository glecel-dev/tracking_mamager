from django.urls import path

from staff.model_views.agent_views import AgentListCreateAPIView, AgentRetrieveUpdateDestroyAPIView, get_groups
from staff.model_views.group_views import GroupListCreateAPIView, GroupRetrieveUpdateDestroyAPIView
from staff.model_views.permissions_views import PermissionListAPIView, get_permissions_table, get_group_permissions
from staff.model_views.permissions_views import PermissionListAPIView, add_permission, \
    remove_permission

urlpatterns = [
    path('group/', GroupListCreateAPIView.as_view(), name='groups'),
    path('group/<int:pk>/', GroupRetrieveUpdateDestroyAPIView.as_view(), name='group'),
    path('agent/', AgentListCreateAPIView.as_view(), name='agents'),
    path('agent/<int:pk>/', AgentRetrieveUpdateDestroyAPIView.as_view(), name='agent'),
    path('get-groups/', get_groups, name='get-groups'),
    path('permission/', PermissionListAPIView.as_view(), name='permissions'),
    path('permission-table/', get_permissions_table, name='permissions-table'),
    path('group-permissions/<int:group_id>/', get_group_permissions, name='group-permissions'),
    path('add/permission/', add_permission, name='add-permission'),
    path('remove/permission/', remove_permission, name='remove-permission'),
]
