# Generated by Django 3.0.2 on 2020-04-23 18:21

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('business_name', models.CharField(max_length=50, verbose_name='Emri')),
                ('nuis', models.CharField(max_length=50, unique=True, verbose_name='NIPT')),
                ('address', models.CharField(max_length=256, verbose_name='Adresa')),
                ('email', models.EmailField(max_length=50, verbose_name='Email')),
                ('phone', models.CharField(blank=True, max_length=50, verbose_name='Telefoni')),
                ('facebook_page_link', models.CharField(blank=True, max_length=100, null=True, verbose_name='Facebook')),
                ('instagram_link', models.CharField(blank=True, max_length=100, null=True, verbose_name='Instagram')),
                ('google_plus_link', models.CharField(blank=True, max_length=100, null=True, verbose_name='Google+')),
                ('twitter_link', models.CharField(blank=True, max_length=100, null=True, verbose_name='Twitter')),
                ('image', models.ImageField(blank=True, null=True, upload_to='company', verbose_name='Zgjidhni imazhin')),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='Created Date')),
                ('date_last_updated', models.DateTimeField(auto_now=True, verbose_name='Last Updated Date')),
                ('deleted', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name': 'Company',
                'verbose_name_plural': 'Companies',
                'db_table': 'sc_company',
                'ordering': ['-date_created'],
            },
        ),
    ]
