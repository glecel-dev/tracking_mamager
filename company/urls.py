from django.urls import path

from company.model_views.company_views import CompanyListCreateAPIView, CompanyRetrieveUpdateDestroyAPIView

urlpatterns = [
    path('company/', CompanyListCreateAPIView.as_view(), name='companies'),
    path('company/<int:pk>/', CompanyRetrieveUpdateDestroyAPIView.as_view(), name='company')
]
