from rest_framework.fields import CharField
from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import Serializer

from company.models import Company


class CompanyReadSerializer(ModelSerializer):
    class Meta:
        model = Company
        exclude = ['date_created', 'date_last_updated', 'deleted']


class CompanyWriteSerializer(ModelSerializer):
    class Meta:
        model = Company
        exclude = ['date_created', 'date_last_updated', 'deleted']


class CompanyFilterSerializer(Serializer):
    business_name = CharField(max_length=50, required=False, allow_blank=True, allow_null=True)
    nuis = CharField(max_length=50, required=False, allow_blank=True, allow_null=True)
