from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from company.models import Company
from company.serializers import CompanyFilterSerializer, CompanyReadSerializer, CompanyWriteSerializer
from service_manager_backend.common.api_views import SMListCreateAPIView, SMRetrieveUpdateDestroyAPIView


class CompanyListCreateAPIView(SMListCreateAPIView):
    queryset = Company.objects.filter(deleted=False)
    read_serializer_class = CompanyReadSerializer
    write_serializer_class = CompanyWriteSerializer
    list_read_serializer_class = CompanyReadSerializer
    filter_serializer_class = CompanyFilterSerializer
    # permission_classes = (IsAuthenticated,)
    filter_map = {}


class CompanyRetrieveUpdateDestroyAPIView(SMRetrieveUpdateDestroyAPIView):
    queryset = Company.objects.filter(deleted=False)
    read_serializer_class = CompanyReadSerializer
    write_serializer_class = CompanyWriteSerializer

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.deleted = True
        instance.save()

        return Response(status=status.HTTP_204_NO_CONTENT)
