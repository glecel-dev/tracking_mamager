import os

from django.db import models

from service_manager_backend.settings import BASE_DIR


class SCModel(models.Model):
    class Meta:
        app_label = 'company'
        abstract = True


class Company(SCModel):
    class Meta:
        verbose_name = 'Company'
        verbose_name_plural = 'Companies'
        db_table = 'sc_company'
        ordering = ['business_name']

    business_name = models.CharField('Emri', max_length=50, blank=False)
    nuis = models.CharField('NIPT', max_length=50, unique=True)
    address = models.CharField('Adresa', max_length=256)
    email = models.EmailField('Email', max_length=50, blank=False)

    # can be blank
    phone = models.CharField('Telefoni', max_length=50, blank=True)
    facebook_page_link = models.CharField('Facebook', max_length=100, blank=True, null=True)
    instagram_link = models.CharField('Instagram', max_length=100, blank=True, null=True)
    google_plus_link = models.CharField('Google+', max_length=100, blank=True, null=True)
    twitter_link = models.CharField('Twitter', max_length=100, blank=True, null=True)
    image = models.ImageField('Zgjidhni imazhin', upload_to='company', blank=True, null=True)

    # auto add
    date_created = models.DateTimeField('Created Date', auto_now_add=True)
    date_last_updated = models.DateTimeField('Last Updated Date', auto_now=True)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.business_name

    def get_logo_absolute_path(self):
        if self.image:
            return self.image.url
        else:
            return os.path.join(BASE_DIR, 'company/static/company/images/SM_logo.png')

    def get_logo_relative_path(self):
        if self.image:
            return '/media/{}'.format(self.image.name)
        else:
            return '/static/company/images/SM_logo.png'
