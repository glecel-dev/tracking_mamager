from django.urls import reverse
from model_bakery import baker
from rest_framework import status

from company.models import Company
from company.tests.utils import get_minimal_data_for_company, get_whole_data_for_company
from service_manager_backend.common.smtestcase import SMAPITestCase
from staff.tests.test_utils import generate_user_token


class TestCompanyListCreateAPIViews(SMAPITestCase):

    @classmethod
    def setUpTestData(cls):
        cls.companies = baker.make(Company, _quantity=15)
        baker.make(Company, _quantity=5, deleted=True)
        cls.url = reverse('companies')

    def setUp(self) -> None:
        # self.authorize_client()
        self.client.credentials(HTTP_AUTHORIZATION='Bearer {}'.format(generate_user_token()))

    def test_can_not_list_create_without_credentials(self):
        self.client.credentials()
        list_response = self.client.get(self.url)
        self.assertEqual(list_response.status_code, status.HTTP_401_UNAUTHORIZED)
        create_response = self.client.post(self.url, data=get_minimal_data_for_company(),
                                           content_type='application/json')
        self.assertEqual(create_response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_company_with_minimal_data(self):
        company_data = get_minimal_data_for_company()
        self.creation_assertions(company_data, should_be_in_response=['id', 'business_name'])

    def test_create_company_with_all_data(self):
        company_data = get_whole_data_for_company()
        self.creation_assertions(company_data, should_be_in_response=['id', 'nuis', 'phone'])

    def test_list_all_not_deleted_companies(self):
        not_deleted_count = Company.objects.filter(deleted=False).count()
        self.list_assertions(paginated=False, response_data_count=not_deleted_count)

    def test_list_paginated_companies(self):
        not_deleted_count = Company.objects.filter(deleted=False).count()
        self.list_assertions(paginated=True, response_data_count=not_deleted_count)

    def test_pagination_has_consistent_results(self):
        first_page_data = self.client.get(self.url, data={'page': 1}).data
        second_page_data = self.client.get(self.url, data={'page': 2}).data

        for first_page_item in first_page_data['data']:
            self.assertNotIn(first_page_item, second_page_data['data'])

    def test_can_filter_companies_by_business_name(self):
        business_name = self.companies[0].business_name
        query_params = {'page': 1, 'flt_business_name': business_name}
        response = self.client.get(self.url, data=query_params)
        filtered_companies_count = Company.objects.filter(deleted=False, business_name__icontains=business_name).count()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data['data']
        self.assertEqual(response.data['pagination']['count'], filtered_companies_count)
        for company in data:
            self.assertIn(business_name, company['business_name'])

    def test_can_filter_companies_by_nuis(self):
        nuis = self.companies[0].nuis
        query_params = {'page': 1, 'flt_nuis': nuis}
        response = self.client.get(self.url, data=query_params)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('data', response.data)
        self.assertEqual(len(response.data['data']), 1)
        self.assertEqual(response.data['pagination']['count'], 1)
        self.assertEqual(response.data['data'][0]['nuis'], nuis)


class TestCompanyRetrieveUpdateDestroy(SMAPITestCase):

    @classmethod
    def setUpTestData(cls):
        cls.url = reverse('companies')
        cls.companies = baker.make(Company, _quantity=2)
        baker.make(Company, _quantity=2, deleted=True)

    def setUp(self):
        self.authorize_client()

    def test_retrieve_company_by_id(self):
        company = self.companies[0]
        url = self.get_url_with_pk(viewname='company', pk=company.id)
        self.retrieve_assertions(url)

    def test_update_company(self):
        company = self.companies[0]
        url = self.get_url_with_pk(viewname='company', pk=company.id)
        update_data = get_whole_data_for_company()
        self.update_assertions(url, update_data, company, should_be_updated=['business_name', 'nuis', 'phone'])

    def test_delete_company_logical(self):
        company = self.companies[-1]
        url = self.get_url_with_pk(viewname='company', pk=company.id)
        self.delete_assertions(url, instance=company, logic=True)
