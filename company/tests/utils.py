from io import BytesIO

from PIL import Image
from django.utils.text import slugify
from faker import Faker

fake = Faker()


def get_minimal_data_for_company():
    return {
        "business_name": fake.company(),
        "nuis": fake.isbn13(separator=''),
        "address": fake.address(),
        "email": fake.ascii_company_email()
        }


def get_created_image():
    image = Image.new("RGBA", size=(200, 200), color=(255, 0, 0, 0))
    image_file = BytesIO(image.tobytes())
    image_file.name = 'test.png'
    image_file.seek(0)
    return image_file


def get_whole_data_for_company():
    return {
        "business_name": fake.company(),
        "nuis": fake.isbn13(separator='-'),
        "address": fake.address(),
        "email": fake.ascii_company_email(),
        "phone": fake.phone_number(),
        "facebook_page_link": f"facebook.com/{slugify(fake.name())}",
        "instagram_link": f"instagram.com/{slugify(fake.name())}",
        "google_plus_link": f"googleplus.com/{slugify(fake.name())}",
        "twitter_link": f"twitter.com/{slugify(fake.name())}"
        }
