import os

from django.test import TestCase
from model_bakery import baker

from company.models import Company
from company.tests.utils import fake
from service_manager_backend.settings import BASE_DIR, AWS_S3_ENDPOINT_URL


class TestCompany(TestCase):

    def setUp(self):
        self.company = baker.make(Company)

    def test_company_create(self):
        """
        Ensure we can create a company by providing only non-nullable fields.
        """
        company = Company.objects.create(
            business_name=fake.company(),
            nuis=fake.isbn13(separator='-'),
            address=fake.address(),
            email=fake.ascii_company_email()
            )
        self.assertNotEqual(company.business_name, None)
        self.assertNotEqual(company.nuis, None)
        self.assertNotEqual(company.address, None)
        self.assertNotEqual(company.email, None)
        self.assertFalse(company.deleted)

    def test_company_update(self):
        """
        Ensure we can update a company.
        """
        new_name = fake.company()
        self.company.business_name = new_name
        self.company.save()
        self.assertEqual(new_name, self.company.business_name)

    def test_company_delete(self):
        self.company.delete()
        self.assertFalse(Company.objects.filter(pk=self.company.id).exists())

    def test_company__str__(self):
        string_repr = self.company.business_name
        self.assertEqual(self.company.__str__(), string_repr)

    def test_get_logo_return_default_image_when_no_image(self):
        self.company.image.delete()
        no_image_abs_path = os.path.join(BASE_DIR, 'company/static/company/images/SM_logo.png')
        logo_absolute_path = self.company.get_logo_absolute_path()

        self.assertEqual(logo_absolute_path, no_image_abs_path)

    def test_get_logo_return_image_when_there_is_an_image(self):
        rel_path = 'company/test_image.jpg'
        self.company.image = rel_path
        self.company.save()

        self.assertTrue(self.company.get_logo_absolute_path().index('https') > -1)

    def test_get_logo_relative_path_when_no_image(self):
        self.company.image.delete()
        no_image_rel_path = '/static/company/images/SM_logo.png'

        self.assertEqual(self.company.get_logo_relative_path(), no_image_rel_path)

    def test_get_logo_relative_path_when_there_is_an_image(self):
        image = 'company/image.jpg'
        self.company.image = image
        self.company.save()
        relative_path = f'/media/{image}'

        self.assertEqual(self.company.get_logo_relative_path(), relative_path)
