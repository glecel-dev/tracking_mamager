from rest_framework.fields import CharField
from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import Serializer

from customer.models import Customer


class CustomerReadSerializer(ModelSerializer):
    class Meta:
        model = Customer
        fields = ['id', 'first_name', 'last_name', 'phone', 'email', 'address']


class CustomerWriteSerializer(ModelSerializer):
    class Meta:
        model = Customer
        fields = ['id', 'first_name', 'last_name', 'phone', 'email', 'address']


class CustomerFilterSerializer(Serializer):
    first_name = CharField(max_length=50, required=False, allow_blank=True, allow_null=True)
    last_name = CharField(max_length=50, required=False, allow_blank=True, allow_null=True)
    phone = CharField(max_length=50, required=False, allow_blank=True, allow_null=True)
    email = CharField(max_length=50, required=False, allow_blank=True, allow_null=True)
