from django.db.models import Q
from django.http import JsonResponse
from rest_framework.decorators import api_view

from django.http import Http404
from rest_framework import status
from rest_framework.response import Response

from customer.models import Customer
from customer.serializers import CustomerFilterSerializer, CustomerReadSerializer, CustomerWriteSerializer
from service_manager_backend.common.action_logger import DELETION
from service_manager_backend.common.api_views import SMListCreateAPIView, SMRetrieveUpdateDestroyAPIView, track_action
from service_manager_backend.cons import ERROR_TYPE, HTTP_404, ERRORS, MESSAGE


class CustomerListCreateAPIView(SMListCreateAPIView):
    queryset = Customer.objects.filter(deleted=False)
    read_serializer_class = CustomerReadSerializer
    write_serializer_class = CustomerWriteSerializer
    filter_serializer_class = CustomerFilterSerializer
    filter_map = {}


class CustomerRetrieveUpdateDestroyAPIView(SMRetrieveUpdateDestroyAPIView):
    queryset = Customer.objects.filter(deleted=False)
    read_serializer_class = CustomerReadSerializer
    write_serializer_class = CustomerWriteSerializer

    def delete(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            instance.deleted = True
            instance.save()
            track_action(request, instance, DELETION)
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Http404 as e:
            response_data = {
                ERROR_TYPE: HTTP_404,
                ERRORS: '{}'.format(e),
                MESSAGE: 'Nuk u gjet'
                }
            response_status = status.HTTP_404_NOT_FOUND
            return Response(response_data, status=response_status)


@api_view(['POST'])
def get_requested_customers(request):
    terms = request.data['terms']
    if terms:
        queryset = Customer.objects.filter(
            Q(deleted=False) & (Q(first_name__icontains=terms) | Q(last_name__icontains=terms)))[:10]
    else:
        queryset = Customer.objects.filter(deleted=False).order_by('first_name')[:10]
    return JsonResponse({'data': [{'id': item['id'], 'name': item['first_name'] + ' ' + item['last_name']} for item in queryset.values('id', 'first_name', 'last_name')]})
