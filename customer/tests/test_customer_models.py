from django.test import TestCase
from faker import Faker
from model_bakery import baker

from customer.models import Customer


class CustomerTest(TestCase):

    def setUp(self):
        self.customer = baker.make(Customer)
        self.fake = Faker()

    def test_customer_create(self):
        self.assertTrue(isinstance(self.customer, Customer))
        self.customer.save()
        self.assertNotEqual(self.customer.first_name, None)
        self.assertTrue(self.customer.id is not None)
        self.assertEqual(self.customer, Customer.objects.get(pk=self.customer.id))

    def test_company_update(self):
        old_name = self.customer.first_name
        self.customer.first_name = self.fake.name()
        self.customer.save()
        self.assertNotEqual(old_name, self.customer.first_name)

    def test_company_delete(self):
        self.customer.delete()
        self.assertFalse(Customer.objects.filter(pk=self.customer.id).exists())
