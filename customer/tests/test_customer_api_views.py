from django.urls import reverse
from faker import Faker
from model_bakery import baker
from rest_framework import status
from rest_framework.test import APITestCase

from customer.models import Customer
from customer.serializers import CustomerWriteSerializer
from staff.tests.test_utils import generate_user_token


class TestCustomerListCreateAPIViews(APITestCase):

    def setUp(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer {}'.format(generate_user_token()))
        self.url = reverse('customers')
        self.faker = Faker()

    def test_customer_list_create(self):
        """
        Client requests to create a customer!
        :return status code returns 201 CREATED!:
        """
        customer = baker.make(Customer)
        customer.phone = self.faker.phone_number()
        customer_serialized = CustomerWriteSerializer(customer).data
        response = self.client.post(self.url, customer_serialized, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        """ 
        Client requests to get the customers list
        :return status code 200 OK!:
        """
        response = self.client.get(self.url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class TestCustomerUpdateDelete(APITestCase):
    def setUp(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer {}'.format(generate_user_token()))
        self.url = reverse('customers')
        self.faker = Faker()
        self.customer = Customer(first_name=self.faker.name(), address=self.faker.address(), email=self.faker.email(), phone=self.faker.phone_number())
        self.customer.save()
        customer_serialized = CustomerWriteSerializer(self.customer).data
        self.customer_created = self.client.post(self.url, customer_serialized, format='json')

    def test_update_customer(self):
        """
        Client request update customer!
        :return status 200 OK!:
        """
        url = reverse('customer', args=[self.customer.id])
        new_values = {
            'first_name': 'testFirstName',
            'last_name': 'testLastName',
            'address': 'testAddress',
            'email': 'test@test.com',
            'phone': '+35512365'
        }
        response = self.client.put(url, new_values, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_customer = Customer.objects.get(pk=self.customer.id)
        self.assertEqual(updated_customer.first_name, new_values['first_name'])
        self.assertEqual(updated_customer.last_name, new_values['last_name'])
        self.assertEqual(updated_customer.address, new_values['address'])
        self.assertEqual(updated_customer.email, new_values['email'])
        self.assertEqual(updated_customer.phone, new_values['phone'])

    def test_retrieve_delete_customer(self):
        """
        Client request to retrieve and delete the customer
        :return 200 OK for retrieve, and 204 NO CONTENT for delete!
        """
        url = reverse('customer', args=[self.customer.id])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
