# Create your models here.
from django.db import models


class SCModel(models.Model):
    class Meta:
        app_label = 'Customer'
        abstract = True


class Customer(SCModel):
    class Meta:
        verbose_name = 'Klient'
        verbose_name_plural = 'Klientë'
        db_table = 'sc_customer'
        ordering = ['first_name', 'last_name']

    first_name = models.CharField('Emri', max_length=50)
    last_name = models.CharField('Mbiemri', max_length=50, blank=True)
    phone = models.CharField('Telefon', max_length=50)
    email = models.EmailField('E-mail', max_length=50, blank=True)
    address = models.CharField('Adresa', max_length=100, blank=True)
    deleted = models.BooleanField(default=False)

    def get_fullname(self):
        return self.first_name + " " + self.last_name

    def __str__(self):
        return self.get_fullname()
