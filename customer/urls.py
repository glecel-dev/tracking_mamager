from django.urls import path

from customer.model_views.customer_views import CustomerListCreateAPIView, CustomerRetrieveUpdateDestroyAPIView, \
    get_requested_customers

urlpatterns = [
    path('customer/', CustomerListCreateAPIView.as_view(), name='customers'),
    path('customer/<int:pk>/', CustomerRetrieveUpdateDestroyAPIView.as_view(), name='customer'),
    path('customer/search/', get_requested_customers, name='customer-search'),
]
