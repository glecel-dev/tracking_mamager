# noinspection PyUnresolvedReferences
from service_manager_backend.settings import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': TEST_DB_NAME,
        'USER': TEST_DB_CREDENTIALS,
        'PASSWORD': TEST_DB_CREDENTIALS,
        'HOST': TEST_DB_HOST,
        'PORT': 5432,
        'TEST': {
            'NAME': TEST_DB_NAME
        }
    }
}
