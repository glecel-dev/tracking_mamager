from django.contrib.auth import password_validation
from staff.models import User
from django.core.exceptions import ValidationError
from rest_framework import status
from rest_framework.generics import UpdateAPIView
from rest_framework.response import Response
from rest_framework_simplejwt.exceptions import InvalidToken, TokenError
from rest_framework_simplejwt.views import TokenObtainPairView

from service_manager_backend.auth.serializers import ChangePasswordSerializer, SMTokenObtainPairSerializer
from service_manager_backend.cons import ERRORS, ERROR_TYPE, FIELD_ERROR, MESSAGE, OTHER, VALIDATION_ERROR


class SMTokenObtainPairView(TokenObtainPairView):
    """
    Takes a set of user credentials and returns an access and refresh JSON web
    token pair to prove the authentication of those credentials.
    """
    serializer_class = SMTokenObtainPairSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        try:
            serializer.is_valid(raise_exception=True)
        except TokenError as to:
            raise InvalidToken(to.args[0])
        except ValidationError as ve:
            return Response(
                {ERROR_TYPE: VALIDATION_ERROR, ERRORS: ve.get_full_details(), MESSAGE: 'Kredenciale të gabuara!'},
                status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({ERROR_TYPE: OTHER, ERRORS: '{}'.format(e), MESSAGE: 'Kredenciale të gabuara!'},
                            status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.validated_data, status=status.HTTP_200_OK)


class ChangePasswordView(UpdateAPIView):
    """
    An endpoint for changing password.
    """
    serializer_class = ChangePasswordSerializer
    model = User

    def update(self, request, *args, **kwargs):
        user = self.request.user
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Check old password
            if not user.check_password(serializer.data.get("old_password")):
                error_response = {
                    ERROR_TYPE: FIELD_ERROR,
                    ERRORS: {'old_password': [{'message': 'Fjalëkalim i vjetër është i gabuar!'}]},
                    MESSAGE: 'Fjalëkalim i vjetër është i gabuar!'
                    }

                return Response(error_response, status=status.HTTP_400_BAD_REQUEST)
            new_password = serializer.data.get("new_password")
            try:
                password_validation.validate_password(password=new_password, user=User)
            except ValidationError as ve:
                error_response = {
                    ERROR_TYPE: FIELD_ERROR,
                    ERRORS: {'new_password': [{'message': ve.messages[0]}]},
                    MESSAGE: ve.messages[0]
                    }

                return Response(error_response, status=status.HTTP_400_BAD_REQUEST)
            user.set_password(new_password)
            user.save()

            return Response({MESSAGE: 'Fjalëkalimi u ndryshua me sukses!'}, status=status.HTTP_200_OK)

        return Response({
            ERROR_TYPE: OTHER,
            ERRORS: serializer.errors,
            MESSAGE: 'Problem i brendshëm'
            }, status=status.HTTP_400_BAD_REQUEST)
