from rest_framework.fields import CharField
from rest_framework.serializers import Serializer
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.tokens import RefreshToken
from six import text_type


class SMTokenObtainPairSerializer(TokenObtainPairSerializer):
    def update(self, instance, validated_data):
        return super(SMTokenObtainPairSerializer, self).update(instance, validated_data)

    def create(self, validated_data):
        return super(SMTokenObtainPairSerializer, self).create(validated_data)

    @classmethod
    def get_token(cls, user):
        return RefreshToken.for_user(user)

    def validate(self, attrs):
        data = super(SMTokenObtainPairSerializer, self).validate(attrs)
        groups = list(self.user.groups.all().values_list('name', flat=True))
        refresh = self.get_token(self.user)
        data['refresh'] = text_type(refresh)
        data['access'] = text_type(refresh.access_token)
        data['groups'] = groups
        data['email'] = self.user.email
        data['permissions'] = list(self.user.get_all_permissions())
        return data


class ChangePasswordSerializer(Serializer):
    """
    Serializer for password change endpoint.
    """
    old_password = CharField(required=True)
    new_password = CharField(required=True)
