"""service_manager_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework_simplejwt.views import TokenRefreshView, TokenVerifyView

from service_manager_backend.auth.views import SMTokenObtainPairView, ChangePasswordView
from staff.views import get_user_info, get_user_permissions
import report.urls as report_urls

urlpatterns = [
    path('admin/', admin.site.urls),

    path('auth/knock-knock/', SMTokenObtainPairView.as_view(), name='knock-knock'),
    path('auth/knock-knock/refresh/', TokenRefreshView.as_view(), name='token-refresh'),
    path('auth/knock-knock/verify/', TokenVerifyView.as_view(), name='token-verify'),
    path('auth/change-password/', ChangePasswordView.as_view(), name='change-password'),
    path('auth/user-info/', get_user_info, name='user-info'),
    path('auth/user-permissions/', get_user_permissions, name='user-info'),

    path('', include('customer.urls')),
    path('', include('warranty.urls')),
    path('', include('company.urls')),
    path('', include('service.urls')),
    path('', include('staff.urls')),
    ]

urlpatterns += [
    path('api-auth/', include('rest_framework.urls')),
]

urlpatterns += report_urls.urlpatterns
