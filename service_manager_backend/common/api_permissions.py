from django.contrib.contenttypes.models import ContentType
from rest_framework.permissions import BasePermission

from service.cons import ADMINISTRATOR_GROUP_ID, ACCEPTED, NOTIFIED_CLIENT, DELIVERED
from service.models import ServiceItem


class CanView(BasePermission):

    def has_permission(self, request, view):
        user = request.user
        obj = ContentType.objects.get_for_model(view.queryset.model)
        return user and user.has_perm(obj.app_label + '.view_' + obj.model)


class CanAdd(BasePermission):

    def has_permission(self, request, view):
        user = request.user
        obj = ContentType.objects.get_for_model(view.queryset.model)
        return user and user.has_perm(obj.app_label + '.add_' + obj.model)


class CanChange(BasePermission):

    def has_permission(self, request, view):
        user = request.user
        obj = ContentType.objects.get_for_model(view.queryset.model)
        return user and user.has_perm(obj.app_label + '.change_' + obj.model)


class CanDelete(BasePermission):

    def has_permission(self, request, view):
        user = request.user
        obj = ContentType.objects.get_for_model(view.queryset.model)
        return user and user.has_perm(obj.app_label + '.delete_' + obj.model)


class CanChangeToInRepair(BasePermission):

    def has_permission(self, request, view):
        if 'id' in request.data:
            service_id = request.data['id']
            service_object = ServiceItem.objects.get(id=service_id)
            obj = ContentType.objects.get_for_model(service_object)
            user = request.user

            if service_object.state == ACCEPTED:
                return user.has_perm(obj.app_label + '.accepted_in_repair')
            elif service_object.state == NOTIFIED_CLIENT:
                return user.has_perm(obj.app_label + '.notified_client_in_repair')
            elif service_object.state == DELIVERED:
                return user.has_perm(obj.app_label + '.delivered_in_repair')
            else:
                return False
        else:
            return False


class CanChangeToRejected(BasePermission):

    def has_permission(self, request, view):
        user = request.user
        if 'id' in request.data:
            service_id = request.data['id']
            service_object = ServiceItem.objects.get(id=service_id)
            obj = ContentType.objects.get_for_model(service_object)
            has_perm = user.has_perm(obj.app_label + '.in_repair_rejected')
            return user and has_perm
        else:
            return False


class CanChangeToDelivered(BasePermission):

    def has_permission(self, request, view):
        user = request.user
        if 'id' in request.data:
            service_id = request.data['id']
            service_object = ServiceItem.objects.get(id=service_id)
            obj = ContentType.objects.get_for_model(service_object)
            has_perm = user.has_perm(obj.app_label + '.notified_client_delivered') or user.has_perm(obj.app_label + '.rejected_delivered')
            return has_perm
        else:
            return False


class CanChangeToNotifiedClient(BasePermission):

    def has_permission(self, request, view):
        user = request.user
        if 'id' in request.data:
            service_id = request.data['id']
            service_object = ServiceItem.objects.get(id=service_id)
            obj = ContentType.objects.get_for_model(service_object)
            has_perm = user.has_perm(obj.app_label + '.in_repair_notified_client')
            return has_perm
        else:
            return False


class IsAdmin(BasePermission):

    def has_permission(self, request, view):
        user = request.user
        return user.is_superuser or user.groups.filter(id=ADMINISTRATOR_GROUP_ID)
