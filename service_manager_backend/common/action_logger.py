from django.contrib.contenttypes.models import ContentType

from common.models import ACActionLogger

ADDITION = 1
CHANGE = 2
DELETION = 3
STATE_CHANGE = 4
CHANGE_PASSWORD = 5
LOGIN = 6
LOGOUT = 7


def log_addition(user_id, user_ip, model_object):
    log_action(user_id, user_ip, model_object, ADDITION, 'Krijoi: {}'.format(model_object._meta.verbose_name.title()))


def log_change(user_id, user_ip, model_object):
    log_action(user_id, user_ip, model_object, CHANGE, 'Ndryshoi: {}'.format(model_object._meta.verbose_name.title()))


def log_deletion(user_id, user_ip, model_object):
    log_action(user_id, user_ip, model_object, DELETION, 'Fshiu: {}'.format(model_object._meta.verbose_name.title()))


def log_state_change(user_id, user_ip, model_object):
    log_action(user_id, user_ip, model_object, STATE_CHANGE, 'Ndryshoi gjëndje: {}'.format(model_object._meta.verbose_name.title()))


def log_change_password(user_id, user_ip, model_object):
    log_action(user_id, user_ip, model_object, CHANGE_PASSWORD, 'Ndryshoi fjalëkalim: {}'.format(model_object._meta.verbose_name.title()))


def log_login(user_id, user_ip, model_object):
    log_action(user_id, user_ip, model_object, LOGIN, 'Login: {}'.format(model_object._meta.verbose_name.title()))


def log_logout(user_id, user_ip, model_object):
    log_action(user_id, user_ip, model_object, LOGOUT, 'Logout: {}'.format(model_object._meta.verbose_name.title()))


def log_action(user_id, user_ip, model_object, action, message):
    ACActionLogger.objects.log_action(user_id=user_id,
                                      user_ip=user_ip,
                                      content_type_id=ContentType.objects.get_for_model(model_object._meta.model).id,
                                      object_id=model_object.id,
                                      object_repr=model_object.__str__(),
                                      action_flag=action,
                                      change_message=message)


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip
