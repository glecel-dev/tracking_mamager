import json

from django.urls import reverse
from model_bakery import baker
from rest_framework import status
from rest_framework.test import APITestCase

from service_manager_backend.auth.serializers import SMTokenObtainPairSerializer
from service_manager_backend.settings import REST_FRAMEWORK
from staff.models import User


class SMAPITestCase(APITestCase):

    @staticmethod
    def get_url_with_pk(viewname, pk):
        return reverse(viewname, kwargs={'pk': pk})

    def authorize_client(self):
        user = baker.make(User)
        token_pair = SMTokenObtainPairSerializer.get_token(user)
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {token_pair.access_token}')

    def creation_assertions(self, posted_data, should_be_in_response=None):
        self.response = self.client.post(self.url, data=json.dumps(posted_data), content_type='application/json')
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)
        self.assertIn('data', self.response.data)
        data = self.response.data['data']
        if should_be_in_response:
            for field in should_be_in_response:
                self.assertIn(field, data)
                if field == 'id':
                    continue
                self.assertEqual(data[field], posted_data[field])

    def list_assertions(self, paginated, response_data_count=None):
        page_size = REST_FRAMEWORK['PAGE_SIZE']
        query_params = None
        if paginated:
            query_params = {'page': 1}
        self.response = self.client.get(self.url, data=query_params)
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.assertIn('data', self.response.data)
        data = self.response.data['data']
        self.assertIsInstance(data, list)

        if paginated:
            self.assertIn('pagination', self.response.data)
            pagination = self.response.data['pagination']
            self.assertLessEqual(len(data), page_size)
            self.assertIn('count', pagination)
            if response_data_count:
                self.assertEqual(pagination['count'], response_data_count)
        else:
            self.assertNotIn('pagination', self.response.data)
            if response_data_count:
                self.assertEqual(len(data), response_data_count)

    def retrieve_assertions(self, url):
        self.response = self.client.get(url)
        pk = int(url.split('/')[-2])
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.assertIn('data', self.response.data)
        data = self.response.data['data']
        self.assertNotIsInstance(data, list)
        self.assertEqual(data['id'], pk)

    def update_assertions(self, url, update_data, instance, should_be_updated=None):
        update_data['id'] = instance.pk
        response = self.client.put(url, data=json.dumps(update_data), content_type='application/json')
        instance.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('data', response.data)
        data = response.data['data']
        self.assertEqual(instance.id, data['id'])

        if should_be_updated is not None:
            for field in should_be_updated:
                self.assertEqual(getattr(instance, field), data.get(field))

    def delete_assertions(self, url, instance, logic=True):
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        if logic:
            instance.refresh_from_db()
            self.assertTrue(instance.deleted)
