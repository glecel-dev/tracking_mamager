from django.db import models
from django.db.models import Sum

from common.models import Counter
from customer.models import Customer
from service.cons import STATE_NAMES, ACCEPTED
from service_manager_backend.settings import DATE_FORMAT
from staff.models import User


class SCModel(models.Model):
    class Meta:
        app_label = 'service'
        abstract = True


class ServiceItem(SCModel):
    class Meta:
        verbose_name = 'Servis'
        verbose_name_plural = 'Servise'
        db_table = 'sc_service_item'
        unique_together = ('code', 'version')
        ordering = ['-code']

    SERVICE_ITEM_CODE = 'SERV'

    code = models.PositiveIntegerField('Kodi', default=0)
    clearance_date = models.DateTimeField('Data e dorëzimit', null=True)
    item_description = models.CharField('Produkti', max_length=200)
    item_reported_issue = models.CharField('Problemi i raportuar nga klienti', max_length=500)
    item_identified_issue = models.CharField('Problemi i identifikuar', blank=True, max_length=500)
    comments = models.CharField('Komente', blank=True, max_length=500)
    customer = models.ForeignKey(Customer, verbose_name='Klienti',
                                 related_name='service_items',
                                 blank=False, on_delete=models.CASCADE)
    rejection_notes = models.CharField('Arsyeja e refuzimit', blank=True, max_length=500)
    different_receiver = models.BooleanField(default=False)
    state = models.IntegerField('Gjendja', blank=False, default=ACCEPTED)
    imei_software = models.CharField('IMEI i sistemit operativ', max_length=50)
    imei_hardware = models.CharField('IMEI i pajisjes', max_length=50)
    switched_on = models.BooleanField('I ndezur', default=False)
    serviced_before = models.BooleanField('Ka qenë në servis më parë', default=False)
    parent = models.ForeignKey('self', related_name='children', blank=True, null=True, on_delete=models.CASCADE)
    version = models.PositiveIntegerField('Versioni', default=0)
    date_created = models.DateTimeField('Data e krijimit', auto_now_add=True)
    date_last_updated = models.DateTimeField('Data e përditësimit të fundit', auto_now=True)
    deleted = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if self.code == 0:
            self.code = Counter.next(self.SERVICE_ITEM_CODE)
        super(ServiceItem, self).save(*args, **kwargs)

    def get_code(self):
        if self.parent:
            return '{}-{}-{}'.format(self.SERVICE_ITEM_CODE, self.code, self.version)
        else:
            return '{}-{}'.format(self.SERVICE_ITEM_CODE, self.code)

    def __str__(self):
        return self.get_code()

    def get_state(self):
        return STATE_NAMES[self.state]

    def get_different_receiver(self):
        if self.different_receiver:
            return 'PO'
        return 'JO'

    def get_switched_on(self):
        if self.switched_on:
            return 'PO'
        return 'JO'

    def get_serviced_before(self):
        if self.serviced_before:
            return 'PO'
        return 'JO'

    def get_service_item_types_total_price(self):
        return self.service_item_service_types.all().aggregate(Sum('price')).get('price__sum', 0)


class ServiceItemStateChange(SCModel):
    class Meta:
        verbose_name = 'Service Item State Change'
        verbose_name_plural = 'Service Items State Changes'
        db_table = 'sc_service_state_change'
        ordering = ['-date_created']

    service_item = models.ForeignKey(ServiceItem, related_name='state_changes', on_delete=models.CASCADE)
    start_state = models.IntegerField()
    end_state = models.IntegerField()
    agent = models.ForeignKey(User, related_name='state_changes', on_delete=models.CASCADE)
    date_created = models.DateTimeField(auto_now_add=True)

    def date_created_str(self):
        return self.date_created.strftime(DATE_FORMAT)

    def get_state(self):
        return STATE_NAMES[self.end_state]


class SparePart(SCModel):
    class Meta:
        verbose_name = 'Spare Part'
        verbose_name_plural = 'Spare Parts'
        db_table = 'sc_spare_part'

    serial_number = models.CharField('Numri serial', max_length=50, blank=False)
    product = models.CharField("Produkti", max_length=150)
    price = models.PositiveIntegerField('Çmimi', blank=False, default=0)
    service_item = models.ForeignKey(ServiceItem, blank=False,
                                     verbose_name='Produkti',
                                     related_name='spare_parts',
                                     on_delete=models.CASCADE)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.serial_number


# Admin Service type!
class ServiceType(SCModel):
    class Meta:
        verbose_name = 'Shërbim'
        verbose_name_plural = 'Shërbime'
        db_table = 'sc_service_type'
        ordering = ['name']

    name = models.CharField('Emërtimi', max_length=120, unique=True)
    default_price = models.PositiveIntegerField('Çmimi i sugjeruar')
    date_created = models.DateTimeField('Data e krijimit', auto_now_add=True)
    date_last_updated = models.DateTimeField('Data e përditësimit të fundit', auto_now=True)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.name


# Service item type, on serviceItem delivered!
class ServiceItemServiceType(SCModel):
    class Meta:
        verbose_name = 'Service Item Service Type'
        verbose_name_plural = 'Service Item Service Types'
        db_table = 'sc_service_item_service_type'

    service_item = models.ForeignKey(ServiceItem, verbose_name='Servisi',
                                     related_name='service_item_service_types', on_delete=models.CASCADE)
    service_type = models.ForeignKey(ServiceType, verbose_name='Shërbimi',
                                     related_name='service_item_service_types', on_delete=models.CASCADE)
    price = models.PositiveIntegerField('Çmimi', default=0)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return "Service item id {} - service type id {}".format(self.service_item.id, self.service_item.id)


class ServiceMessage(SCModel):
    class Meta:
        verbose_name = 'Service Message'
        verbose_name_plural = 'Service Messages'
        db_table = 'sc_service_message'
        ordering = ['-date_created']

    date_created = models.DateTimeField(auto_now_add=True)
    message = models.CharField("Mesazhi", max_length=512)
    agent = models.ForeignKey(User, related_name='service_messages', on_delete=models.CASCADE)
    service = models.ForeignKey(ServiceItem, related_name='service_messages', on_delete=models.CASCADE)
    read = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)


class Reminder(SCModel):
    class Meta:
        verbose_name = 'Kujtesë'
        verbose_name_plural = 'Kujtesa'
        db_table = 'sc_reminder'
        ordering = ['-date_reminding']

    notes = models.CharField('Shënime', max_length=1024)
    date_reminding = models.DateTimeField('Data')
    service = models.ForeignKey(ServiceItem, related_name='reminders', on_delete=models.CASCADE)
    checked = models.BooleanField(default=False)
    user = models.ForeignKey(User, related_name='reminders', on_delete=models.CASCADE)
    date_created = models.DateTimeField('Created Date', auto_now_add=True)
    date_last_updated = models.DateTimeField('Last Updated Date', auto_now=True)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.notes
