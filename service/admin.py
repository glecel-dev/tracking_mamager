from django.contrib import admin

# Register your models here.
from service.models import ServiceType, Reminder, ServiceItem, ServiceItemStateChange, SparePart, ServiceItemServiceType, ServiceMessage

admin.site.register(ServiceItem)
admin.site.register(ServiceItemStateChange)
admin.site.register(SparePart)
admin.site.register(ServiceType)
admin.site.register(ServiceItemServiceType)
admin.site.register(ServiceMessage)
admin.site.register(Reminder)
