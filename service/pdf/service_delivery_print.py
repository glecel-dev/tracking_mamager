import logging
import os

from reportlab.lib import colors, utils
from reportlab.lib.enums import TA_CENTER, TA_RIGHT, TA_LEFT
from reportlab.lib.pagesizes import A5
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import cm, mm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus import Paragraph, Image, KeepTogether
from reportlab.platypus import Table
from reportlab.platypus import TableStyle, Spacer, SimpleDocTemplate

from service.pdf.custom_canvas import CustomCanvas
from service_manager_backend.settings import DATE_FORMAT

__author__ = 'ItWorks'

logger = logging.getLogger(__name__)
dir_path = os.path.dirname(os.path.realpath(__file__))
font_path = os.path.join(dir_path, 'hp_simplified')


class ServiceDeliveryPage:
    font_family = 'Hp-Simplified'
    font_family_bold = 'Hp-Simplified-Bold'
    font_family_italic = 'Hp-Simplified-Italic'
    font_normal_leading = 10.8
    font_small_size = 10
    spacer_width = 1 * cm
    spacer_height = 0.25 * cm
    spacer = KeepTogether(Spacer(spacer_width, spacer_height))
    middle_spacer = KeepTogether(Spacer(spacer_width, 0.50 * cm))
    large_spacer = KeepTogether(Spacer(spacer_width, 1 * cm))
    styles = getSampleStyleSheet()
    title_style = ParagraphStyle(name='sc-title', parent=styles['Normal'], fontName=font_family_bold, fontSize=15, leading=14.4, alignment=TA_CENTER, wordWrap='LTR')
    normal_title_style = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontName=font_family_bold, fontSize=font_small_size, leading=font_normal_leading, wordWrap='LTR', spaceBefore=5, alignment=TA_CENTER)
    normal_style = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontName=font_family, fontSize=font_small_size, leading=font_normal_leading, wordWrap='LTR')
    normal_style_right = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontSize=font_small_size, leading=font_normal_leading, alignment=TA_RIGHT, wordWrap='LTR', spaceAfter=5)
    address_normal_style_right = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontSize=8.5, leading=8, alignment=TA_RIGHT, wordWrap='LTR', spaceAfter=5)
    last_normal_style_right = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontName=font_family, fontSize=font_small_size, leading=20, alignment=TA_RIGHT, wordWrap='LTR', spaceAfter=5)
    normal_style_center = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontName=font_family, fontSize=font_small_size, leading=font_normal_leading, alignment=TA_CENTER, wordWrap='LTR')
    normal_style_left = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontSize=font_small_size, leading=font_normal_leading, alignment=TA_LEFT, wordWrap='LTR')
    centered_small_style = ParagraphStyle(name='sc-small', leftIndent=5, parent=styles['Normal'], fontName=font_family, fontSize=10, leading=font_normal_leading, alignment=TA_CENTER, wordWrap='LTR')
    left_small_style = ParagraphStyle(name='sc-small', leftIndent=5, parent=styles['Normal'], fontName=font_family, fontSize=10, leading=font_normal_leading, alignment=TA_LEFT, wordWrap='LTR')
    normal_style_left_bold = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontName=font_family_bold, fontSize=font_small_size, leading=font_normal_leading, alignment=TA_LEFT, wordWrap='LTR')
    normal_style_right_bold = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontName=font_family_bold, fontSize=font_small_size, leading=font_normal_leading, alignment=TA_RIGHT, wordWrap='LTR')
    paragraph_title_style = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontName=font_family_bold, fontSize=font_small_size, leading=font_normal_leading, wordWrap='LTR', spaceBefore=5, spaceAfter=10)
    bordered_normal_style = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontName=font_family, fontSize=font_small_size, leading=font_normal_leading, wordWrap='LTR', borderColor='#000000', borderWidth=1, spaceAfter=15, borderPadding=(5, 2, 15))
    pdfmetrics.registerFont(TTFont("Hp-Simplified", os.path.join(font_path, 'HP-SIMPLIFIED-REGULAR.ttf')))
    pdfmetrics.registerFont(TTFont("Hp-Simplified-Bold", os.path.join(font_path, 'HP-SIMPLIFIED-BOLD.ttf')))
    pdfmetrics.registerFont(TTFont("Hp-Simplified-Italic", os.path.join(font_path, 'HP-SIMPLIFIED-ITALIC.ttf')))

    table_common_style = TableStyle([
        ('FONTNAME', (0, 0), (-1, 0), font_family_bold),
        ('FONTNAME', (0, 1), (-1, -1), font_family),
        ('FONTSIZE', (0, 1), (-3, -3), font_small_size),
        ('LEADING', (0, 1), (-1, -1), font_normal_leading),
        ('LEFTPADDING', (0, 0), (-1, -1), 0),
        ('RIGHTPADDING', (0, 0), (-1, -1), 0),
        ('LINEABOVE', (0, 1), (-1, 1), 0.5, colors.grey),
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
        ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
        ('GRID', (0, 0), (-1, -1), 0.5, colors.black),
        ('BACKGROUND', (0, 0), (-1, 0), colors.Color(0.6, 0.6, 0.6)),
    ])

    service_item_types_container_table_style = TableStyle([
        ('FONTNAME', (0, 0), (-1, 0), font_family_bold),
        ('FONTNAME', (0, 1), (-1, -1), font_family),
        ('FONTSIZE', (0, 1), (-3, -3), font_small_size),
        ('LEADING', (0, 1), (-1, -1), font_normal_leading),
        ('LEFTPADDING', (0, 0), (-1, -1), 0),
        ('RIGHTPADDING', (0, 0), (-1, -1), 0),
        ('LINEABOVE', (0, 1), (-1, 1), 0.5, colors.grey),
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
        ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
        ('GRID', (0, 0), (-1, -1), 0.5, colors.black),
        ('BACKGROUND', (0, 0), (-1, 0), colors.Color(0.6, 0.6, 0.6)),
        ('SPAN', (0, -1), (3, -1)),
    ])

    top_page_margin = 0.55 * cm
    bottom_page_margin = 3 * cm
    left_page_margin = right_page_margin = 0.5 * cm

    def __init__(self, data, company):
        self.data = data
        self.company = company
        self.title = data.get_code()
        # self.spare_part_table_info = [['Përshkimi', 'Numri Serial', 'Vlera(ALL)']]
        # if hasattr(self.data, 'parts'):
        #     self.prepare_spare_parts(self.data.parts.all())

    def get_phones(self, data):
        phones = []
        for phone in data:
            phones.append(Paragraph(phone, style=self.address_normal_style_right))
        return phones

    def service_item_types_container(self):

        if hasattr(self.data, 'service_item_service_types'):
            service_types = self.data.service_item_service_types.all()
        else:
            service_types = []
        table = [[
            Paragraph('Nr.', style=self.normal_style_center),
            Paragraph('Produkti/ <br/> Shërbimi', style=self.normal_style_center),
            Paragraph('Njësia', style=self.normal_style_center),
            Paragraph('Sasia', style=self.normal_style_center),
            Paragraph('Vlefta <br/> pa TVSH', style=self.normal_style_center),
            Paragraph('Vlefta <br/> e TVSH', style=self.normal_style_center),
            Paragraph('Vlefta <br/> me TVSH', style=self.normal_style_center),
        ]]

        vpt_total = 0
        vat_total = 0

        for index, service_type in enumerate(service_types):
            vpt = service_type.price / 1.2
            vpt_value = '{:.2f}'.format(round(vpt, 2))
            vat = vpt * 0.2
            vat_value = '{:,.2f}'.format(round(vat, 2))
            vpt_total += vpt
            vat_total += vat
            table.append([
                Paragraph(str(index + 1), style=self.normal_style_center),
                Paragraph(str(service_type.service_type.name), style=self.normal_style_center),
                Paragraph('-', style=self.normal_style_center),
                Paragraph('-', style=self.normal_style_center),
                Paragraph(str(vpt_value), style=self.normal_style_center),
                Paragraph(str(vat_value), style=self.normal_style_center),
                Paragraph(str(service_type.price), style=self.normal_style_center),
            ])

        total_row = [
            Paragraph('<b>Totali</b>', self.centered_small_style),
            Paragraph('', self.normal_style),
            Paragraph('', self.normal_style),
            Paragraph('', self.normal_style),
            Paragraph(str('{:,.2f}'.format(round(vpt_total, 2))), self.normal_style_center),
            Paragraph(str('{:,.2f}'.format(round(vat_total, 2))), self.normal_style_center),
            Paragraph(str(self.data.get_service_item_types_total_price()), self.normal_style_center),
        ]

        table.append(total_row)

        return table

    # def prepare_spare_parts(self, spare_parts):
    #     for spare_part in spare_parts:
    #         append_content = [
    #             Paragraph(str(spare_part.product), style=self.left_small_style),
    #             Paragraph(str(spare_part.serial_number), style=self.centered_small_style),
    #             Paragraph(str(spare_part.price), style=self.centered_small_style)
    #         ]
    #         self.spare_part_table_info.append(append_content)

    def print_page(self, filename):
        flowables = self.header()
        flowables += self.body()
        simple_doc_template = SimpleDocTemplate(filename, title=self.title, pagesize=A5, topMargin=self.top_page_margin, rightMargin=self.right_page_margin, bottomMargin=self.bottom_page_margin, leftMargin=self.left_page_margin, spaceAfter=6)
        simple_doc_template.build(flowables, canvasmaker=CustomCanvas, onFirstPage=self.footer, onLaterPages=self.footer)

    def footer(self, temp_canvas, _):
        temp_canvas.setFont("HPSimplifiedW02-Regular", 9)
        temp_canvas.drawString(7 * mm, 10 * mm, "Fletë dorëzimi servisi")
        temp_canvas.setFont("HPSimplifiedW02-Italic", 9)
        # if self.company.facebook_page_link:
        #     temp_canvas.drawImage(FACEBOOK_ICON, 7 * mm, 23 * mm, mask='auto')
        #     temp_canvas.drawString(13 * mm, 23 * mm, self.company.facebook_page_link)
        # if self.company.google_plus_link:
        #     temp_canvas.drawImage(GOOGLE_PLUS_ICON, 7 * mm, 16 * mm, mask='auto')
        #     temp_canvas.drawString(13 * mm, 16 * mm, self.company.google_plus_link)
        # if self.company.twitter_link:
        #     temp_canvas.drawImage(TWITTER_ICON, 100 * mm, 16 * mm, mask='auto')
        #     temp_canvas.drawRightString(115 * mm, 16 * mm, self.company.twitter_link)
        # if self.company.instagram_link:
        #     temp_canvas.drawImage(INSTAGRAM_ICON, 100 * mm, 23 * mm, mask='auto')
        #     temp_canvas.drawRightString(115 * mm, 23 * mm, self.company.instagram_link)

    def header(self):
        header_table_style = TableStyle([
            ("LINEBELOW", (0, 0), (-1, -1), 1, colors.black),
            ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
            ('LEADING', (0, 0), (-1, -1), 20),
        ])
        img = utils.ImageReader(self.company.get_logo_absolute_path())
        iw, ih = img.getSize()
        aspect = iw / float(ih)
        logo = Image(self.company.get_logo_absolute_path(), height=60, width=60 * aspect, hAlign='LEFT')
        address = Paragraph(self.company.address, style=self.address_normal_style_right)
        phone_numbers = self.get_phones(self.company.phone.split('|'))
        email = Paragraph(self.company.email, style=self.address_normal_style_right)
        nuis = Paragraph('NIPT: {}'.format(self.company.nuis), style=self.address_normal_style_right)
        header_table = [
            [logo, [address, phone_numbers, email, nuis]]
            # ['', [address, phone_numbers, email, nuis]]
        ]
        header_table_data = Table(header_table, colWidths='*', style=header_table_style)
        return [header_table_data]

    def body(self):
        title = Paragraph('<b>Fletë dorëzimi servisi</b>', style=self.title_style)
        customer = Paragraph("Klienti: <b>{}</b>".format(self.data.customer.get_fullname()), style=self.normal_style_left)
        date_created = Paragraph("Data e regjistrimit: <b>{}</b>".format(self.data.date_created.strftime(DATE_FORMAT)), style=self.normal_style_right)
        customer_phone = Paragraph("Telefoni: <b>{}</b>".format(self.data.customer.phone), style=self.normal_style_left)
        code = Paragraph("Kodi: <b>{}</b>".format(str(self.data.get_code())), style=self.normal_style_right)
        serviced_before = Paragraph("Ka qënë në servis më parë: <b>{}</b>".format(self.data.get_serviced_before()), style=self.normal_style_left)
        switched_on = Paragraph("I ndezur: <b>{}</b>".format(self.data.get_switched_on()), style=self.normal_style_right)
        product_t = Paragraph("Produkti: <b>{}</b>".format(str(self.data.item_description)), style=self.normal_style_left)
        imei_hardware = Paragraph("IMEI i pajisjes: <b>{}</b>".format(str(self.data.imei_hardware if self.data.imei_hardware else '')), style=self.normal_style_left)
        identified_issue_title_h = Paragraph("Problemi i konstatuar: <b>{}</b>".format(str(self.data.item_identified_issue)), style=self.normal_style_left)
        comments_title_h = Paragraph("Komente mbi punën e kryer: <b>{}</b>".format(str(self.data.comments)), style=self.normal_style_left)
        service_table_info = [
            [customer, date_created],
            [customer_phone, code],
            [serviced_before, switched_on],
            [product_t, ''],
            [imei_hardware, ''],
            [identified_issue_title_h, ''],
            [comments_title_h, '']
        ]

        service_table = Table(service_table_info, colWidths="*", style=TableStyle([
            ('LEFTPADDING', (0, 0), (-1, -1), 0),
            ('SPAN', (0, 3), (1, 3)),
            ('SPAN', (0, 4), (1, 4)),
            ('SPAN', (0, 5), (1, 5)),
            ('SPAN', (0, 6), (1, 6)),
        ]))

        labour_cost = Paragraph("<b>Vlera e shërbimit: </b> {} ALL".format(''), style=self.normal_style_right)

        service_discount = Paragraph("<b>Ulje e aplikuar: {} ALL</b> ".format(''), style=self.normal_style_right)

        service_total_cost = Paragraph("<b>TOTAL: <u>{} ALL</u></b> ".format(''), style=self.normal_style_right)

        fixed_text_data = "Në bazë te ligjit Nr.9887, datë 10.03.2008, 'PËR MBROJTJEN E TE DHËNAVE PERSONALE'," \
                          " të dhënat tuaja do të përdoren për asnjë arsye tjetër përveç se për qëllimin identifikues në rast kërkese për asistencë teknike"

        fixed_text = Paragraph(fixed_text_data, style=self.normal_style_left_bold)

        business_hours_title = Paragraph('Orari i punës', style=self.normal_style_center)

        business_hours = Paragraph('09:00 - 21:00', style=self.normal_title_style)

        # if len(self.spare_part_table_info) > 1:
        #     spare_part_label = Paragraph("<b>Artikujt e këmbyer: </b>", style=self.paragraph_title_style)
        #     spare_part = Table(self.spare_part_table_info, colWidths='*', style=self.table_common_style)
        #     return [
        #         self.middle_spacer, title, self.spacer, self.spacer, service_table,
        #         self.spacer, self.spacer, labour_cost, spare_part_label, spare_part, self.spacer, service_discount, service_total_cost, self.middle_spacer, fixed_text, business_hours_title,
        #         business_hours
        #     ]

        if len(self.service_item_types_container()) > 2:
            service_type_title = Paragraph("<b> Shërbime: </b>", style=self.paragraph_title_style)
            service_type_table = Table(self.service_item_types_container(), colWidths='*', style=self.service_item_types_container_table_style)
            return [
                self.middle_spacer, title, self.spacer, self.spacer, service_table,
                self.spacer, self.spacer, service_type_title, service_type_table, self.spacer, fixed_text, business_hours_title,
                business_hours
            ]
        else:
            return [
                self.middle_spacer, title, self.spacer, self.spacer, service_table,
                self.spacer, self.spacer, labour_cost, self.spacer, self.spacer, service_discount, service_total_cost, self.middle_spacer, fixed_text, business_hours_title,
                business_hours
            ]
