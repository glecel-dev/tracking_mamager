import base64

from django.http import HttpResponse
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from company.models import Company
from service.models import ServiceItem
from service.pdf.service_acceptance_print import ServiceAcceptancePage
from service.pdf.service_delivery_print import ServiceDeliveryPage


@api_view(['GET'])
def print_service_acceptance(request, service_id):
    service = ServiceItem.objects.get(id=service_id)
    company = Company.objects.first()
    service_page = ServiceAcceptancePage(service, company)
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'inline; filename="{}.pdf"'.format(service.get_code())
    service_page.print_page(response)
    return Response({'data': base64.b64encode(response.content).decode(), 'title': service.get_code()},
                    status=status.HTTP_200_OK)


@api_view(['GET'])
def print_service_delivery(request, service_id):
    service = ServiceItem.objects.get(id=service_id)
    company = Company.objects.first()
    service_page = ServiceDeliveryPage(service, company)
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'inline; filename="{}.pdf"'.format(service.get_code())
    service_page.print_page(response)
    return Response({'data': base64.b64encode(response.content).decode(), 'title': service.get_code()},
                    status=status.HTTP_200_OK)
