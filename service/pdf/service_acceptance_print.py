import logging
import os

from reportlab.lib import colors, utils
from reportlab.lib.enums import TA_CENTER, TA_LEFT, TA_RIGHT
from reportlab.lib.pagesizes import A5
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import cm, mm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus import Paragraph, Image, KeepTogether
from reportlab.platypus import Table
from reportlab.platypus import TableStyle, Spacer, SimpleDocTemplate

from service_manager_backend.settings import DATE_FORMAT, FACEBOOK_ICON, GOOGLE_PLUS_ICON, TWITTER_ICON, INSTAGRAM_ICON
from service.pdf.custom_canvas import CustomCanvas

__author__ = 'ItWorks'

logger = logging.getLogger(__name__)

dir_path = os.path.dirname(os.path.realpath(__file__))
font_path = os.path.join(dir_path, 'hp_simplified')


class ServiceAcceptancePage:
    font_family = 'Hp-Simplified'
    font_family_bold = 'Hp-Simplified-Bold'
    font_family_italic = 'Hp-Simplified-Italic'
    font_normal_leading = 11
    font_small_size = 10
    spacer_width = 1 * cm
    spacer_height = 0.25 * cm
    spacer = KeepTogether(Spacer(spacer_width, spacer_height))
    middle_spacer = KeepTogether(Spacer(spacer_width, 0.50 * cm))
    large_spacer = KeepTogether(Spacer(spacer_width, 1 * cm))
    styles = getSampleStyleSheet()
    title_style = ParagraphStyle(name='sc-title', parent=styles['Normal'], fontName=font_family_bold, fontSize=15, leading=14.4, alignment=TA_CENTER, wordWrap='LTR')
    normal_title_style = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontName=font_family_bold, fontSize=font_small_size, leading=font_normal_leading, wordWrap='LTR', spaceBefore=5, alignment=TA_CENTER)
    normal_style_left = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontSize=font_small_size, leading=font_normal_leading, alignment=TA_LEFT, wordWrap='LTR')
    normal_style_center = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontName=font_family, fontSize=font_small_size, leading=font_normal_leading, alignment=TA_CENTER, wordWrap='LTR')
    # normal_style_right = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontName=font_family, fontSize=font_small_size, leading=font_normal_leading, alignment=TA_RIGHT, wordWrap='LTR')
    address_normal_style_right = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontSize=8.5, leading=8, alignment=TA_RIGHT, wordWrap='LTR', spaceAfter=5)
    normal_style_right = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontSize=font_small_size, leading=font_normal_leading, alignment=TA_RIGHT, wordWrap='LTR', spaceAfter=5)
    last_normal_style_right = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontName=font_family, fontSize=font_small_size, leading=20, alignment=TA_RIGHT, wordWrap='LTR', spaceAfter=5)
    paragraph_title_style = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontName=font_family_bold, fontSize=font_small_size, leading=font_normal_leading, wordWrap='LTR', spaceBefore=5)
    bordered_normal_style = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontName=font_family, fontSize=font_small_size, leading=font_normal_leading, wordWrap='LTR', borderColor='#000000', borderWidth=0.5,
                                           leftIndent=5.5, rightIndent=5.5, borderPadding=(1, 5, 15))
    normal_style_left_bold = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontName=font_family_bold, fontSize=font_small_size, leading=font_normal_leading, alignment=TA_LEFT, wordWrap='LTR')
    normal_style_right_bold = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontName=font_family_bold, fontSize=font_small_size, leading=font_normal_leading, alignment=TA_RIGHT, wordWrap='LTR')

    pdfmetrics.registerFont(TTFont("Hp-Simplified", os.path.join(font_path, 'HP-SIMPLIFIED-REGULAR.ttf')))
    pdfmetrics.registerFont(TTFont("Hp-Simplified-Bold", os.path.join(font_path, 'HP-SIMPLIFIED-BOLD.ttf')))
    top_page_margin = 0.55 * cm
    bottom_page_margin = 1 * cm
    left_page_margin = right_page_margin = 0.5 * cm

    def __init__(self, data, company):
        self.data = data
        self.title = data.get_code()
        self.company = company

    def get_phones(self, data):
        phones = []
        for phone in data:
            phones.append(Paragraph(phone, style=self.address_normal_style_right))
        return phones

    def header(self):
        header_table_style = TableStyle([
            ("LINEBELOW", (0, 0), (-1, -1), 1, colors.black),
            ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
            ('LEADING', (0, 0), (-1, -1), 20),
        ])
        img = utils.ImageReader(self.company.get_logo_absolute_path())
        iw, ih = img.getSize()
        aspect = iw / float(ih)
        logo = Image(self.company.get_logo_absolute_path(), height=60, width=60 * aspect, hAlign='LEFT')
        address = Paragraph(self.company.address, style=self.address_normal_style_right)
        phone_numbers = self.get_phones(self.company.phone.split('|'))
        email = Paragraph(self.company.email, style=self.address_normal_style_right)
        nuis = Paragraph('NIPT: {}'.format(self.company.nuis), style=self.address_normal_style_right)
        header_table = [
            [logo, [address, phone_numbers, email, nuis]]
        ]
        header_table_data = Table(header_table, colWidths='*', style=header_table_style)
        return [header_table_data]

    def body(self):
        title = Paragraph("Fletë pranimi servisi", style=self.title_style)
        customer = Paragraph("Klienti: <b>{}</b>".format(self.data.customer.get_fullname()), style=self.normal_style_left)
        date_created = Paragraph("Data e regjistrimit: <b>{}</b>".format(self.data.date_created.strftime(DATE_FORMAT)), style=self.normal_style_right)
        customer_phone = Paragraph("Telefoni: <b>{}</b>".format(self.data.customer.phone), style=self.normal_style_left)
        code = Paragraph("Kodi: <b>{}</b>".format(str(self.data.get_code())), style=self.normal_style_right)
        serviced_before = Paragraph("Ka qënë në servis më parë: <b>{}</b>".format(self.data.get_serviced_before()), style=self.normal_style_left)
        switched_on = Paragraph("I ndezur: <b>{}</b>".format(self.data.get_switched_on()), style=self.normal_style_right)
        product_t = Paragraph("Produkti: <b>{}</b>".format(str(self.data.item_description)), style=self.normal_style_left)
        issue_reported_h = Paragraph("Përshkrimi i problemit: <b>{}</b>".format(str(self.data.item_reported_issue)), style=self.normal_style_left)
        service_table_info = [
            [customer, date_created],
            [customer_phone, code],
            [serviced_before, switched_on],
            [product_t, ''],
            [issue_reported_h, ''],
        ]

        service_table = Table(service_table_info, colWidths="*", style=TableStyle([
            ('LEFTPADDING', (0, 0), (-1, -1), 0),
            ('SPAN', (0, 3), (1, 3)),
            ('SPAN', (0, 4), (1, 4)),
        ]))

        fixed_text_data = "Në bazë te ligjit Nr.9887, datë 10.03.2008, 'PËR MBROJTJEN E TE DHËNAVE PERSONALE'," \
                          " të dhënat tuaja do të përdoren për asnjë arsye tjetër përveç se për qëllimin identifikues në rast kërkese për asistencë teknike"
        fixed_text = Paragraph(fixed_text_data, style=self.normal_style_left_bold)
        business_hours_title = Paragraph('Orari i punës', style=self.normal_style_center)
        business_hours = Paragraph('09:00 - 21:00', style=self.normal_title_style)
        return [
            self.spacer, title, self.middle_spacer, service_table, self.middle_spacer,
            self.spacer, self.large_spacer, fixed_text, business_hours_title, business_hours
        ]

    def footer(self, temp_canvas, _):
        temp_canvas.setFont("HPSimplifiedW02-Regular", 9)
        temp_canvas.drawString(7 * mm, 10 * mm, "Fletë pranimi servisi")
        temp_canvas.setFont("HPSimplifiedW02-Italic", 9)
        if self.company.facebook_page_link:
            temp_canvas.drawImage(FACEBOOK_ICON, 7 * mm, 23 * mm, mask='auto')
            temp_canvas.drawString(13 * mm, 23 * mm, self.company.facebook_page_link)
        if self.company.google_plus_link:
            temp_canvas.drawImage(GOOGLE_PLUS_ICON, 7 * mm, 16 * mm, mask='auto')
            temp_canvas.drawString(13 * mm, 16 * mm, self.company.google_plus_link)
        if self.company.twitter_link:
            temp_canvas.drawImage(TWITTER_ICON, 100 * mm, 16 * mm, mask='auto')
            temp_canvas.drawRightString(115 * mm, 16 * mm, self.company.twitter_link)
        if self.company.instagram_link:
            temp_canvas.drawImage(INSTAGRAM_ICON, 100 * mm, 23 * mm, mask='auto')
            temp_canvas.drawRightString(115 * mm, 23 * mm, self.company.instagram_link)

    def print_page(self, filename):
        flowables = self.header()
        flowables += self.body()
        simple_doc_template = SimpleDocTemplate(filename, title=self.title, pagesize=A5, topMargin=self.top_page_margin, rightMargin=self.right_page_margin, bottomMargin=self.bottom_page_margin,
                                                leftMargin=self.left_page_margin)
        simple_doc_template.build(flowables, canvasmaker=CustomCanvas, onFirstPage=self.footer, onLaterPages=self.footer)
