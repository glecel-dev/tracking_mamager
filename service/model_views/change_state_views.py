import datetime
import json

from django.db.transaction import atomic
from django.http import JsonResponse

from django.http.response import HttpResponseBadRequest, HttpResponseBase
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from service.cons import ACCEPTED, DELIVERED, IN_REPAIR, NOTIFIED_CLIENT, REJECTED
from service.models import ServiceItem, ServiceItemServiceType, ServiceItemStateChange, SparePart
from service_manager_backend.common.action_logger import get_client_ip, log_addition, log_state_change
from service_manager_backend.common.api_permissions import CanChangeToInRepair, CanChangeToRejected, CanChangeToDelivered, IsAdmin, CanChangeToNotifiedClient

BAD_REQUEST_CODE = HttpResponseBadRequest.status_code
BASE_REQUEST_CODE = HttpResponseBase.status_code


@api_view(['POST'])
@permission_classes([CanChangeToInRepair])
@atomic
def change_to_in_repair(request):
    service_id = request.data['id']
    item_identified_issue = request.data['item_identified_issue']
    service_object = ServiceItem.objects.get(id=service_id)
    if service_object.state == ACCEPTED:
        service_object.state = IN_REPAIR
        service_object.item_identified_issue = item_identified_issue
        service_object.save()
        log_addition(request.user.id, get_client_ip(request), service_object)
        service_item_state_change = ServiceItemStateChange(service_item_id=service_object.id,
                                                           start_state=ACCEPTED,
                                                           end_state=IN_REPAIR,
                                                           agent_id=request.user.id)
        service_item_state_change.save()
    else:
        new_service_object = ServiceItem(code=service_object.code,
                                         item_description=service_object.item_description,
                                         item_reported_issue=service_object.item_reported_issue,
                                         item_identified_issue=item_identified_issue,
                                         imei_hardware=service_object.imei_hardware,
                                         imei_software=service_object.imei_software,
                                         serviced_before=service_object.serviced_before,
                                         switched_on=service_object.switched_on,
                                         state=IN_REPAIR,
                                         customer_id=service_object.customer_id,
                                         parent_id=service_object.id,
                                         version=service_object.version + 1)
        new_service_object.save()
        service_object.deleted = True
        service_object.save()
        service_item = ServiceItem.objects.get(pk=service_id).state_changes.filter(end_state=IN_REPAIR)
        agent_id = service_item.latest('date_created').agent_id
        log_addition(request.user.id, get_client_ip(request), new_service_object)
        service_item_state_change = ServiceItemStateChange(service_item_id=new_service_object.id,
                                                           start_state=service_object.state,
                                                           end_state=new_service_object.state,
                                                           agent_id=agent_id)
        service_item_state_change.save()
        change_service_items_types_service_id(service_object, int(new_service_object.id))

    return Response({}, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([CanChangeToRejected])
@atomic
def change_to_rejected(request):
    service_id = request.data['id']
    rejection_notes = request.data['rejection_notes']
    updated_object = ServiceItem.objects.get(id=service_id)
    current_state = updated_object.state
    updated_object.state = REJECTED
    updated_object.rejection_notes = rejection_notes
    updated_object.save()
    log_state_change(request.user.id, get_client_ip(request), updated_object)
    service_item_state_change = ServiceItemStateChange(service_item_id=updated_object.id,
                                                       start_state=current_state,
                                                       end_state=updated_object.state,
                                                       agent_id=request.user.id)
    service_item_state_change.save()
    return Response({}, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([CanChangeToNotifiedClient])
@atomic
def change_to_notified_client(request):
    service_id = request.data['id']
    comments = request.data['comments']
    updated_object = ServiceItem.objects.get(id=service_id)
    current_state = updated_object.state
    updated_object.state = NOTIFIED_CLIENT
    updated_object.comments = comments
    updated_object.save()
    log_state_change(request.user.id, get_client_ip(request), updated_object)
    service_item_state_change = ServiceItemStateChange(service_item_id=updated_object.id,
                                                       start_state=current_state,
                                                       end_state=updated_object.state,
                                                       agent_id=request.user.id)
    service_item_state_change.save()
    if request.data['spare_parts']:
        spare_parts = request.data['spare_parts']
        for spare_part in spare_parts:
            if spare_part['price'] < 0:
                return Response({}, status=status.HTTP_403_FORBIDDEN)
            if spare_part['price'] > 0:
                new_spare_part = SparePart(service_item_id=service_id,
                                           serial_number=spare_part['serial_number'],
                                           product=spare_part['product'],
                                           price=spare_part['price'])
                new_spare_part.save()
        return Response({}, status=status.HTTP_200_OK)
    else:
        return Response({}, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([CanChangeToDelivered])
@atomic
def change_to_delivered(request):
    service_id = request.data['id']
    different_receiver = request.data['different_receiver']
    updated_object = ServiceItem.objects.get(id=service_id)
    current_state = updated_object.state
    updated_object.state = DELIVERED
    updated_object.clearance_date = datetime.datetime.now()
    updated_object.different_receiver = different_receiver
    updated_object.save()
    if 'spare_parts_data' in request.data:
        spare_parts = request.data['spare_parts_data']
        for spare_part in spare_parts:
            updated_spare_part = SparePart.objects.get(id=spare_part['id'])
            updated_spare_part.price = spare_part['price'] if spare_part['price'] else 0
            updated_spare_part.save()
    # Service item types!
    if 'service_item_types' in request.data:
        if len(request.data['service_item_types']) > 0:
            ServiceItem.objects.get(pk=int(service_id)).service_item_service_types.all().delete()
            service_item_types = request.data['service_item_types']
            service_item_queryset_data = []
            for service_item_type in service_item_types:
                service_item_queryset_data.append(
                    ServiceItemServiceType(
                        service_item_id=updated_object.id,
                        service_type_id=int(service_item_type['service_type_id']),
                        price=int(service_item_type['price']))
                )
            ServiceItemServiceType.objects.bulk_create(service_item_queryset_data)
    log_state_change(request.user.id, get_client_ip(request), updated_object)
    service_item_state_change = ServiceItemStateChange(service_item_id=updated_object.id,
                                                       start_state=current_state,
                                                       end_state=updated_object.state,
                                                       agent_id=request.user.id)
    service_item_state_change.save()
    return Response({}, status=status.HTTP_200_OK)


def change_service_items_types_service_id(old_service_item, new_service_id):
    service_item_queryset_data = []
    for service_item_type in old_service_item.service_item_service_types.filter(service_item_id=old_service_item.id):
        service_item_queryset_data.append(ServiceItemServiceType(service_item_id=new_service_id,
                                                                 service_type_id=service_item_type.service_type.id,
                                                                 price=service_item_type.price))
        service_item_type.delete()
    ServiceItemServiceType.objects.bulk_create(service_item_queryset_data)


@api_view(['POST'])
@permission_classes([IsAdmin])
@atomic
def change_service_agent(request):
    data = request.data
    service_id = data['id']
    agent_id = data['agent']
    service_item = ServiceItem.objects.get(pk=service_id)
    service_item_state_change = ServiceItemStateChange(service_item_id=service_id,
                                                       start_state=service_item.state,
                                                       end_state=IN_REPAIR,
                                                       agent_id=agent_id)
    service_item_state_change.save()
    return JsonResponse(data={}, status=status.HTTP_200_OK)
