from service.model_serializers.reminder_serializer import ReminderReadSerializer, ReminderWriteSerializer, \
    ReminderFilterSerializer
from service.models import Reminder
from service_manager_backend.common.api_views import SMListCreateAPIView, SMRetrieveUpdateDestroyAPIView


class ReminderListCreateAPIView(SMListCreateAPIView):
    queryset = Reminder.objects.filter(deleted=False)
    read_serializer_class = ReminderReadSerializer
    write_serializer_class = ReminderWriteSerializer
    list_read_serializer_class = ReminderReadSerializer
    filter_serializer_class = ReminderFilterSerializer
    # permission_classes = (CanView, CanAdd, CanChange, CanDelete)
    filter_map = {}


class ReminderRetrieveUpdateDestroyAPIView(SMRetrieveUpdateDestroyAPIView):
    queryset = Reminder.objects.filter(deleted=False)
    read_serializer_class = ReminderReadSerializer
    write_serializer_class = ReminderWriteSerializer
