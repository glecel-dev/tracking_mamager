from django.db.models import Q
from django.http import Http404
from django.http import JsonResponse
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from service.model_serializers.service_type_serializer import ServiceTypeFilterSerializer, ServiceTypeReadSerializer, \
    ServiceTypeWriteSerializer
from service.models import ServiceType
from service_manager_backend.common.action_logger import DELETION
from service_manager_backend.common.api_views import SMListCreateAPIView, SMRetrieveUpdateDestroyAPIView, track_action
from service_manager_backend.cons import ERRORS, ERROR_TYPE, HTTP_400, HTTP_404, MESSAGE


class ServiceTypeListCreateAPIView(SMListCreateAPIView):
    queryset = ServiceType.objects.filter(deleted=False)
    read_serializer_class = ServiceTypeReadSerializer
    write_serializer_class = ServiceTypeWriteSerializer
    list_read_serializer_class = ServiceTypeReadSerializer
    filter_serializer_class = ServiceTypeFilterSerializer
    filter_map = {}


class ServiceTypeRetrieveUpdateDestroyAPIView(SMRetrieveUpdateDestroyAPIView):
    queryset = ServiceType.objects.filter(deleted=False)
    read_serializer_class = ServiceTypeReadSerializer
    write_serializer_class = ServiceTypeWriteSerializer

    def delete(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            if instance.service_item_service_types.filter():
                response_data = {
                    ERROR_TYPE: HTTP_400,
                    ERRORS: 'Nuk lejohet fshirja e Sherbimit',
                    MESSAGE: 'Nuk lejohet fshirja e Sherbimit'
                }
                return Response(response_data, status=status.HTTP_400_BAD_REQUEST)
            else:
                track_action(request, instance, DELETION)
                self.get_object().delete()
                return Response(status=status.HTTP_200_OK)
        except Http404 as e:
            response_data = {
                ERROR_TYPE: HTTP_404,
                ERRORS: '{}'.format(e),
                MESSAGE: 'Nuk u gjet'
                }
            response_status = status.HTTP_404_NOT_FOUND
            return Response(response_data, status=response_status)


@api_view(['POST'])
def get_requested_service_type(request):
    terms = request.data['terms']
    if terms:
        queryset = ServiceType.objects.filter(
            Q(deleted=False) & (Q(name__icontains=terms)))
    else:
        queryset = ServiceType.objects.filter(deleted=False).order_by('name')
    return JsonResponse({'data': [{'id': item['id'], 'name': item['name'], 'price': item['default_price']} for item in queryset.values('id', 'name', 'default_price')]})
