import datetime

from django.contrib.admin.models import DELETION
from django.core.mail import EmailMessage
from django.db.models import Q
from django.http import Http404, JsonResponse
from django.template.loader import render_to_string
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from service.mail_conf import EMAIL_HOST_USER
from service.model_serializers.service_item_serializers import ServiceItemFilterSerializer, ServiceItemReadSerializer, \
    ServiceItemWriteSerializer
from service.models import Reminder
from service.models import ServiceItem
from service_manager_backend.common.api_views import SMListCreateAPIView, SMRetrieveUpdateDestroyAPIView, track_action
from service_manager_backend.cons import ERRORS, ERROR_TYPE, HTTP_404, MESSAGE
from service_manager_backend.settings import DATE_TIME_FORMAT
from staff.models import User


class ServiceItemListCreateAPIView(SMListCreateAPIView):
    queryset = ServiceItem.objects.filter(deleted=False)
    read_serializer_class = ServiceItemReadSerializer
    write_serializer_class = ServiceItemWriteSerializer
    list_read_serializer_class = ServiceItemReadSerializer
    filter_serializer_class = ServiceItemFilterSerializer
    filter_map = {}


class ServiceItemRetrieveUpdateDestroyAPIView(SMRetrieveUpdateDestroyAPIView):
    queryset = ServiceItem.objects.filter(deleted=False)
    read_serializer_class = ServiceItemReadSerializer
    write_serializer_class = ServiceItemWriteSerializer

    def delete(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            instance.deleted = True
            instance.save()
            track_action(request, instance, DELETION)
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Http404 as e:
            response_data = {
                ERROR_TYPE: HTTP_404,
                ERRORS: '{}'.format(e),
                MESSAGE: 'Nuk u gjet'
                }
            response_status = status.HTTP_404_NOT_FOUND
            return Response(response_data, status=response_status)


# Send reminder emails!
def check_reminders():
    time_now = datetime.datetime.now()
    year = time_now.year
    month = time_now.month
    day = time_now.day
    hour = time_now.hour
    minute = time_now.minute
    reminders = Reminder.objects.filter(deleted=False, date_reminding__year=year, date_reminding__month=month,
                                        date_reminding__day=day, date_reminding__hour=hour,
                                        date_reminding__minute=minute)
    for reminder in reminders:
        context_data = {'title': reminder.service.get_code(), 'user': reminder.user, 'notes': reminder.notes}
        send_mail(reminder.user.email, "Kujtesë - {}".format(reminder.date_reminding.strftime(DATE_TIME_FORMAT)),
                  render_to_string('service/reminder_email_tmpl.html', context=context_data))


def send_mail(receiver, subject, html_content):
    msg = EmailMessage(subject, html_content, EMAIL_HOST_USER, [receiver])
    msg.content_subtype = "html"  # Main content is now text/html
    msg.send()


@api_view(['GET'])
def get_agents(request):
    queryset = User.objects.all().order_by('first_name')
    return JsonResponse(
        {'data': [
            {'id': item.id,
             'name': item.first_name + " " + item.last_name
             } for item in queryset]})


@api_view(['POST'])
def get_agent_list(request):
    terms = request.data['terms']
    if terms:
        queryset = User.objects.filter(
            Q(email__icontains=terms) | Q(first_name__icontains=terms) | Q(
                last_name__icontains=terms))
    else:
        queryset = User.objects.filter(is_superuser=False).order_by('first_name')

    return JsonResponse(
        {'data': [
            {'id': item.id,
             'name': item.first_name + " " + item.last_name
             } for item in queryset]})
