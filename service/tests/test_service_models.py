from django.test import TestCase
from faker import Faker
from model_bakery import baker

from service.models import Reminder, ServiceItem, ServiceItemServiceType, ServiceItemStateChange, ServiceMessage, \
    SparePart


class TestServiceItem(TestCase):
    def setUp(self):
        self.service = baker.make(ServiceItem)
        self.fake = Faker()

    def test_company_create(self):
        self.assertTrue(isinstance(self.service, ServiceItem))
        self.service.save()
        self.assertNotEqual(self.service.item_description, None)
        self.assertTrue(self.service.id is not None)
        self.assertEqual(self.service, ServiceItem.objects.get(pk=self.service.id))

    def test_company_update(self):
        old_name = self.service.item_description
        self.service.item_description = self.fake.name()
        self.service.save()
        self.assertNotEqual(old_name, self.service.item_description)

    def test_company_delete(self):
        self.service.delete()
        self.assertFalse(ServiceItem.objects.filter(pk=self.service.id).exists())


class TestServiceItemStateChange(TestCase):
    def setUp(self):
        self.service = baker.make(ServiceItemStateChange)

    def test_company_create(self):
        self.assertTrue(isinstance(self.service, ServiceItemStateChange))
        self.service.save()
        self.assertNotEqual(self.service.service_item, None)
        self.assertTrue(self.service.id is not None)
        self.assertEqual(self.service, ServiceItemStateChange.objects.get(pk=self.service.id))

    def test_company_update(self):
        old_service_item = self.service.service_item
        self.service.service_item = baker.make(ServiceItem)
        self.service.save()
        self.assertNotEqual(old_service_item, self.service.service_item)

    def test_company_delete(self):
        self.service.delete()
        self.assertFalse(ServiceItemStateChange.objects.filter(pk=self.service.id).exists())


class TestSparePart(TestCase):
    def setUp(self):
        self.service = baker.make(SparePart)
        self.fake = Faker()

    def test_company_create(self):
        self.assertTrue(isinstance(self.service, SparePart))
        self.service.save()
        self.assertNotEqual(self.service.service_item, None)
        self.assertTrue(self.service.id is not None)
        self.assertEqual(self.service, SparePart.objects.get(pk=self.service.id))

    def test_company_update(self):
        old_service_item = self.service.service_item
        self.service.service_item = baker.make(ServiceItem)
        self.service.save()
        self.assertNotEqual(old_service_item, self.service.service_item)

    def test_company_delete(self):
        self.service.delete()
        self.assertFalse(SparePart.objects.filter(pk=self.service.id).exists())


class TestServiceItemServiceType(TestCase):
    def setUp(self):
        self.service = baker.make(ServiceItemServiceType)
        self.fake = Faker()

    def test_company_create(self):
        self.assertTrue(isinstance(self.service, ServiceItemServiceType))
        self.service.save()
        self.assertNotEqual(self.service.service_item, None)
        self.assertTrue(self.service.id is not None)
        self.assertEqual(self.service, ServiceItemServiceType.objects.get(pk=self.service.id))

    def test_company_update(self):
        old_service_item = self.service.service_item
        self.service.service_item = baker.make(ServiceItem)
        self.service.save()
        self.assertNotEqual(old_service_item, self.service.service_item)

    def test_company_delete(self):
        self.service.delete()
        self.assertFalse(ServiceItemServiceType.objects.filter(pk=self.service.id).exists())


class TestServiceMessage(TestCase):
    def setUp(self):
        self.service = baker.make(ServiceMessage)
        self.fake = Faker()

    def test_company_create(self):
        self.assertTrue(isinstance(self.service, ServiceMessage))
        self.service.save()
        self.assertNotEqual(self.service.service, None)
        self.assertTrue(self.service.id is not None)
        self.assertEqual(self.service, ServiceMessage.objects.get(pk=self.service.id))

    def test_company_update(self):
        old_service_item = self.service.service
        self.service.service = baker.make(ServiceItem)
        self.service.save()
        self.assertNotEqual(old_service_item, self.service.service)

    def test_company_delete(self):
        self.service.delete()
        self.assertFalse(ServiceMessage.objects.filter(pk=self.service.id).exists())


class TestReminder(TestCase):
    def setUp(self):
        self.reminder = baker.make(Reminder)
        self.fake = Faker()

    def test_company_create(self):
        self.assertTrue(isinstance(self.reminder, Reminder))
        self.reminder.save()
        self.assertNotEqual(self.reminder.service, None)
        self.assertTrue(self.reminder.id is not None)
        self.assertEqual(self.reminder, Reminder.objects.get(pk=self.reminder.id))

    def test_company_update(self):
        old_service_item = self.reminder.service
        self.reminder.service = baker.make(ServiceItem)
        self.reminder.save()
        self.assertNotEqual(old_service_item, self.reminder.service)

    def test_company_delete(self):
        self.reminder.delete()
        self.assertFalse(Reminder.objects.filter(pk=self.reminder.id).exists())
