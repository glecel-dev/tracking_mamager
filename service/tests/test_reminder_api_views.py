from django.urls import reverse
from faker import Faker
from model_bakery import baker
from rest_framework import status
from rest_framework.test import APITestCase

from service.models import Reminder, ServiceItem
from service_manager_backend.auth.serializers import SMTokenObtainPairSerializer
from staff.tests.test_utils import generate_user_token


class TestReminderListCreateAPIViews(APITestCase):

    def setUp(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer {}'.format(generate_user_token()))
        self.url = reverse('reminders')
        self.faker = Faker()

    def test_reminder_list_create(self):
        """
        Client requests to create a reminder!
        :return status code 201 CREATED!
        """
        service = baker.make(ServiceItem)
        reminder = {
            "service": service.id,
            "date_reminding": self.faker.date_time(),
            "notes": self.faker.sentence(nb_words=6, variable_nb_words=True, ext_word_list=None),
            # "user": self.user.id
        }
        response = self.client.post(self.url, reminder, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client.get(self.url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class TestReminderUpdateDelete(APITestCase):
    def setUp(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer {}'.format(generate_user_token()))
        self.url = reverse('reminders')
        self.faker = Faker()
        self.reminder = baker.make(Reminder)

    def test_update_reminder(self):
        """
        Client requestes update reminder!
        :return status 200 OK!
        """
        object_id = self.reminder.id
        url = reverse('reminder', args=[object_id])
        service = baker.make(ServiceItem)
        new_values = {
            'notes': 'test',
            'date_reminding': self.faker.date_time(),
            'service': service.id
        }
        response = self.client.put(url, new_values, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_reminder = Reminder.objects.get(pk=object_id)
        self.assertEqual(updated_reminder.notes, 'test')
        self.assertEqual(str(updated_reminder.service), service.__str__())
