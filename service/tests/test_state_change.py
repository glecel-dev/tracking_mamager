from django.urls import reverse
from faker import Faker
from model_bakery import baker
from rest_framework import status
from rest_framework.test import APITestCase

from customer.models import Customer
from service.cons import ACCEPTED, DELIVERED, IN_REPAIR, NOTIFIED_CLIENT, REJECTED
from service.models import ServiceItem, ServiceItemStateChange, ServiceType, SparePart
from staff.tests.test_utils import generate_user_token


class TestChangeToInRepair(APITestCase):

    def setUp(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer {}'.format(generate_user_token()))
        self.url = reverse('change-to-in-repair')
        self.faker = Faker()
        self.customer = baker.make(Customer)
        self.service_item = ServiceItem(customer=self.customer,
                                        item_description=self.faker.pystr(max_chars=50),
                                        imei_hardware=self.faker.pystr(max_chars=50),
                                        imei_software=self.faker.pystr(max_chars=50),
                                        item_reported_issue=self.faker.pystr(max_chars=500))
        self.service_item.save()
        self.data = {
            "id": self.service_item.id,
            "item_identified_issue": self.faker.sentence(nb_words=6, variable_nb_words=True, ext_word_list=None)
        }

    def test_request_method_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_request_method_put(self):
        response = self.client.put(self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_request_method_delete(self):
        response = self.client.delete(self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_request_method_post(self):
        response = self.client.post(self.url, self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        service_item = ServiceItem.objects.get(id=self.service_item.id)
        self.assertEqual(service_item.state, IN_REPAIR)
        self.assertEqual(service_item.item_identified_issue, self.data["item_identified_issue"])
        self.assertTrue(ServiceItemStateChange.objects.get(service_item=service_item))

    def test_state_is_not_accepted(self):
        # state_changes = baker.make(ServiceItemStateChange)
        # state_changes.service_item = ServiceItem.objects.get(id=self.service_item.id)
        self.service_item.state = IN_REPAIR
        self.client.post(self.url, self.data, format='json')
        self.assertEqual(self.service_item.state, IN_REPAIR)
        new_service_item = ServiceItem.objects.get(item_identified_issue=self.data['item_identified_issue'])
        print(self.service_item.state)
        self.assertEqual(self.service_item.id, new_service_item.id)
        self.assertTrue(ServiceItemStateChange.objects.get(service_item=self.service_item))


class TestChangeToRejected(APITestCase):

    def setUp(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer {}'.format(generate_user_token()))
        self.url = reverse('change-to-rejected')
        self.faker = Faker()
        self.customer = baker.make(Customer)
        self.service_item = ServiceItem(customer=self.customer,
                                        item_description=self.faker.pystr(max_chars=50),
                                        imei_hardware=self.faker.pystr(max_chars=50),
                                        imei_software=self.faker.pystr(max_chars=50),
                                        item_reported_issue=self.faker.pystr(max_chars=500),
                                        item_identified_issue=self.faker.pystr(max_chars=500))
        self.service_item.save()
        self.data = {
            "id": self.service_item.id,
            "rejection_notes": self.faker.sentence(nb_words=6, variable_nb_words=True, ext_word_list=None),
            "state": ACCEPTED
        }

    def test_request_method_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_request_method_put(self):
        response = self.client.put(self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_request_method_delete(self):
        response = self.client.delete(self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_request_method_post(self):
        response = self.client.post(self.url, self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        service_item = ServiceItem.objects.get(id=self.service_item.id)
        self.assertEqual(service_item.state, REJECTED)
        self.assertEqual(service_item.rejection_notes, self.data["rejection_notes"])
        self.assertTrue(ServiceItemStateChange.objects.get(service_item=self.service_item))


class TestChangeToNotifyClient(APITestCase):

    def setUp(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer {}'.format(generate_user_token()))
        self.url = reverse('change-to-notified-client')
        self.faker = Faker()
        self.customer = baker.make(Customer)
        self.service_item = ServiceItem(customer=self.customer,
                                        item_description=self.faker.pystr(max_chars=50),
                                        imei_hardware=self.faker.pystr(max_chars=50),
                                        imei_software=self.faker.pystr(max_chars=50),
                                        item_reported_issue=self.faker.pystr(max_chars=500),
                                        item_identified_issue=self.faker.pystr(max_chars=500))
        self.service_item.save()
        self.data = {
            "id": self.service_item.id,
            "comments": self.faker.sentence(nb_words=6, variable_nb_words=True, ext_word_list=None),
            "state": IN_REPAIR,
            "spare_parts": [

                {
                    "product": self.faker.sentence(nb_words=6, variable_nb_words=True, ext_word_list=None),
                    "price": 555,
                    "serial_number": "6a4f3e43d1a31f"
                }

            ]
        }

    def test_request_method_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_request_method_put(self):
        response = self.client.put(self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_request_method_delete(self):
        response = self.client.delete(self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_request_method_post(self):
        response = self.client.post(self.url, self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        service_item = ServiceItem.objects.get(id=self.service_item.id)
        self.assertEqual(service_item.state, NOTIFIED_CLIENT)
        self.assertEqual(service_item.comments, self.data["comments"])
        #         test the creation of the spare parts
        self.assertTrue(SparePart.objects.get(serial_number='6a4f3e43d1a31f'))
        self.assertTrue(ServiceItemStateChange.objects.get(service_item=self.service_item))

    def test_for_the_negative_price(self):
        data = {
            "id": self.service_item.id,
            "comments": self.faker.sentence(nb_words=6, variable_nb_words=True, ext_word_list=None),
            "state": IN_REPAIR,
            "spare_parts": [
                {
                    "product": self.faker.sentence(nb_words=6, variable_nb_words=True, ext_word_list=None),
                    "price": -55,
                    "serial_number": "6a4f3e43d1a31l"
                }
            ]
        }
        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertTrue(ServiceItemStateChange.objects.get(service_item=self.service_item))


class TestChangeToDelivered(APITestCase):

    def setUp(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer {}'.format(generate_user_token()))
        self.url = reverse('change-to-delivered')
        self.faker = Faker()
        self.customer = baker.make(Customer)
        self.service_item = ServiceItem(customer=self.customer,
                                        item_description=self.faker.pystr(max_chars=50),
                                        imei_hardware=self.faker.pystr(max_chars=50),
                                        imei_software=self.faker.pystr(max_chars=50),
                                        item_reported_issue=self.faker.pystr(max_chars=500),
                                        item_identified_issue=self.faker.pystr(max_chars=500),
                                        state=NOTIFIED_CLIENT,
                                        )
        self.service_item.save()
        self.spare_part = SparePart(serial_number='a4df6a8e46fads',
                                    product='some product',
                                    price=15000,
                                    service_item=self.service_item)
        self.spare_part.save()
        self.service_type = ServiceType(name="test", default_price=2000)
        self.service_type.save()
        self.data = {
            "id": self.service_item.id,
            "different_receiver": True,
            "state": NOTIFIED_CLIENT,
            "spare_parts": [
                {
                    "id": self.spare_part.id,
                    "price": self.spare_part.price,
                }
            ],
            "service_item_types": [
                {
                    "service_type_id": self.service_type.id,
                    "price": self.faker.pyint()
                }

            ]
        }

    def test_request_method_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_request_method_put(self):
        response = self.client.put(self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_request_method_delete(self):
        response = self.client.delete(self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_request_method_post(self):
        response = self.client.post(self.url, self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        service_item = ServiceItem.objects.get(id=self.service_item.id)
        self.assertEqual(service_item.state, DELIVERED)
        self.assertEqual(service_item.different_receiver, self.data["different_receiver"])
        self.assertEqual(service_item.spare_parts.first().price, self.data['spare_parts'][0]['price'])
        self.assertEqual(service_item.service_item_service_types.first().price,
                         self.data['service_item_types'][0]['price'])
        #         test the creation of the spare parts
        # self.assertTrue(SparePart.objects.get(serial_number='a4df6a8e46fads'))
        # self.assertEqual(SparePart.objects.get(id=1).price, 15000)
        # self.assertTrue(ServiceItemStateChange.objects.get(service_item=self.service_item))
