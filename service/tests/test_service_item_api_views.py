from django.urls import reverse
from faker import Faker
from model_bakery import baker
from rest_framework import status
from rest_framework.test import APITestCase

from customer.models import Customer
from service.models import ServiceItem
from staff.tests.test_utils import generate_user_token


class TestServiceItemListCreateAPIViews(APITestCase):

    def setUp(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer {}'.format(generate_user_token()))
        self.url = reverse('service-items')
        self.faker = Faker()

    def test_service_item_list_create(self):
        """
        Client requests to create a service item!
        :return status code returns 201 CREATED!
        Client requests to get the service item
        :return status code 200 OK!
        """
        service_item = {
            "customer": baker.make(Customer).id,
            "item_description": "test",
            "imei_hardware": "test",
            "imei_software": "test",
            "switched_on": False,
            "serviced_before": False,
            "item_reported_issue": "test"
        }
        response = self.client.post(self.url, service_item, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.get(self.url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class TestServiceItemUpdateDelete(APITestCase):

    def setUp(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer {}'.format(generate_user_token()))
        self.url = reverse('service-items')
        self.faker = Faker()
        self.service_item = {
            "customer": baker.make(Customer).id,
            "item_description": "test",
            "imei_hardware": "test",
            "imei_software": "test",
            "switched_on": False,
            "serviced_before": False,
            "item_reported_issue": "test"
        }
        self.service_item_created = self.client.post(self.url, self.service_item, format='json')

    def test_update_service_item(self):
        """
        Client request update service item!
        :return status 200 OK!:
        """
        object_id = ServiceItem.objects.get(item_description='test', imei_software='test').id
        url = reverse('service-item', args=[object_id])
        new_values = {
            'item_description': 'testtest',
            'item_reported_issue': 'testtest',
            'customer': baker.make(Customer).id,
            'imei_software': 'test',
            'imei_hardware': 'test'
        }
        response = self.client.put(url, new_values, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_service_item = ServiceItem.objects.get(pk=object_id)
        self.assertEqual(updated_service_item.item_description, 'testtest')
        self.assertEqual(updated_service_item.item_reported_issue, 'testtest')
