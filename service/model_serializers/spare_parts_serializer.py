from rest_framework.fields import CharField
from rest_framework.serializers import ModelSerializer, Serializer

from service.models import SparePart


class SparePartReadSerializer(ModelSerializer):
    class Meta:
        model = SparePart
        fields = [
            'id', 'serial_number', 'product', 'price'
            ]


class SparePartWriteSerializer(ModelSerializer):
    class Meta:
        model = SparePart
        fields = [
            'id', 'serial_number', 'product', 'price'
            ]


class ServiceTypeFilterSerializer(Serializer):
    name = CharField(required=False, allow_null=True)
