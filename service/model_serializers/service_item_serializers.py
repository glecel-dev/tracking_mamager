from rest_framework import serializers
from rest_framework.fields import CharField, IntegerField
from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import Serializer

from service.model_serializers.spare_parts_serializer import SparePartReadSerializer
from service.models import ServiceItem, ServiceItemStateChange


class ServiceItemReadSerializer(ModelSerializer):
    customer_fullname = serializers.SerializerMethodField()
    spare_parts_data = serializers.SerializerMethodField()
    customer_phone = serializers.ReadOnlyField(source='customer.phone')
    code = serializers.ReadOnlyField(source='get_code')
    creator = serializers.SerializerMethodField()
    service_item_service_type = serializers.SerializerMethodField()

    class Meta:
        model = ServiceItem
        fields = [
            'id', 'code', 'date_created', 'customer', 'customer_fullname', 'customer_phone',
            'item_description', 'item_reported_issue', 'item_identified_issue', 'imei_hardware',
            'imei_software', 'switched_on', 'different_receiver', 'serviced_before', 'spare_parts_data',
            'state', 'state_changes', 'get_state', 'get_service_item_types_total_price',
            'get_different_receiver', 'get_switched_on', 'get_serviced_before', 'comments', 'date_last_updated',
            'rejection_notes', 'creator', 'service_item_service_type'
            ]

    def get_service_item_service_type(self, obj):
        list_array = obj.service_item_service_types.all().values_list('service_type__name', 'price')
        obj_list = []
        for list in list_array:
            obj_list.append({'name': list[0], 'price': list[1]})

        return obj_list

    def get_spare_parts_data(self, obj):
        return SparePartReadSerializer(obj.spare_parts, many=True).data if obj.spare_parts else []

    def get_customer_fullname(self, obj):
        return obj.customer.get_fullname() if obj.customer else ''

    #ToDo - ??
    def get_creator(self, obj):
        return obj.state_changes.order_by('-id').first().agent.get_full_name() if obj.state_changes.all() else ''


class ServiceItemWriteSerializer(ModelSerializer):
    class Meta:
        model = ServiceItem
        fields = [
            'customer', 'item_description', 'imei_hardware', 'imei_software', 'switched_on',
            'serviced_before', 'item_reported_issue', 'item_identified_issue'
            ]


class ServiceItemFilterSerializer(Serializer):
    code = IntegerField(required=False, allow_null=True)
    item_description = CharField(max_length=50, required=False, allow_blank=True, allow_null=True)
    customer = IntegerField(required=False)
    customer__phone = CharField(max_length=50, required=False, allow_blank=True, allow_null=True)
    state = IntegerField(required=False)
