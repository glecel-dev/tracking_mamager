from rest_framework import serializers
from rest_framework.fields import DateTimeField
from rest_framework.relations import StringRelatedField
from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import Serializer

from service.models import Reminder


class ReminderReadSerializer(ModelSerializer):
    service_code = StringRelatedField(source='service')

    class Meta:
        model = Reminder
        fields = ['id', 'date_reminding', 'service', 'service_code', 'notes']


class ReminderWriteSerializer(ModelSerializer):
    class Meta:
        model = Reminder
        fields = ['id', 'service', 'date_reminding', 'notes']

    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        return super(ReminderWriteSerializer, self).create(validated_data)


class ReminderFilterSerializer(Serializer):
    date_reminding_min = serializers.DateField(required=False, allow_null=True)
    date_reminding_max = serializers.DateField(required=False, allow_null=True)
