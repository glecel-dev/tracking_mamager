from rest_framework.fields import CharField
from rest_framework.serializers import ModelSerializer, Serializer

from service.models import ServiceType


class ServiceTypeReadSerializer(ModelSerializer):
    class Meta:
        model = ServiceType
        fields = ['id', 'name', 'default_price']


class ServiceTypeWriteSerializer(ModelSerializer):
    class Meta:
        model = ServiceType
        fields = ['id', 'name', 'default_price']


class ServiceTypeFilterSerializer(Serializer):
    name = CharField(required=False, allow_null=True)
