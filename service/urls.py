from django.urls import path

from service.model_views.change_state_views import change_to_in_repair, change_to_rejected, \
    change_to_notified_client, \
    change_to_delivered, change_service_agent
from service.model_views.reminder_views import ReminderListCreateAPIView, ReminderRetrieveUpdateDestroyAPIView
from service.model_views.service_item_views import ServiceItemListCreateAPIView, \
    ServiceItemRetrieveUpdateDestroyAPIView, get_agent_list, get_agents
from service.pdf.pdf_views import print_service_acceptance, print_service_delivery
from service.model_views.service_type_views import ServiceTypeListCreateAPIView, \
    ServiceTypeRetrieveUpdateDestroyAPIView, get_requested_service_type

urlpatterns = [
    path('service-item/', ServiceItemListCreateAPIView.as_view(), name='service-items'),
    path('service-item/<int:pk>/', ServiceItemRetrieveUpdateDestroyAPIView.as_view(), name='service-item'),
    path('reminder/', ReminderListCreateAPIView.as_view(), name='reminders'),
    path('reminder/<int:pk>/', ReminderRetrieveUpdateDestroyAPIView.as_view(), name='reminder'),
    path('service-type/', ServiceTypeListCreateAPIView.as_view(), name='service-types'),
    path('service-type/<int:pk>/', ServiceTypeRetrieveUpdateDestroyAPIView.as_view(), name='service-type'),
    path('service-type/search/', get_requested_service_type, name='service-type-search'),
    path('agent/search/', get_agent_list, name='agent-search'),
    path('get-agents/', get_agents, name='get-agents'),


    path('change-to-in-repair/', change_to_in_repair, name='change-to-in-repair'),
    path('change-to-rejected/', change_to_rejected, name='change-to-rejected'),
    path('change-to-notified_client/', change_to_notified_client, name='change-to-notified-client'),
    path('change-to-delivered/', change_to_delivered, name='change-to-delivered'),
    path('change-service-agent/', change_service_agent, name='change-service-agent'),

    path('print/acceptance/<int:service_id>/', print_service_acceptance, name='print-acceptance'),
    path('print/delivery/<int:service_id>/', print_service_delivery, name='print-delivery'),
]
