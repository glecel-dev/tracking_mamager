import os

from django.utils import timezone
from reportlab.lib.colors import HexColor
from reportlab.lib.units import mm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfgen import canvas

from service_manager_backend.settings import DATE_FORMAT

dir_path = os.path.dirname(os.path.realpath(__file__))
font = os.path.dirname(os.path.realpath(__file__))
afmFileRegular = os.path.join(font, 'hp_simplified/HP-SIMPLIFIED-REGULAR.afm')
pfbFileRegular = os.path.join(font, 'hp_simplified/HP-SIMPLIFIED-REGULAR.pfb')
afmFileItalic = os.path.join(font, 'hp_simplified/HP-SIMPLIFIED-ITALIC.afm')
pfbFileItalic = os.path.join(font, 'hp_simplified/HP-SIMPLIFIED-ITALIC.pfb')


class CustomCanvas(canvas.Canvas):
    # ----------------------------------------------------------------------
    def __init__(self, *args, **kwargs):
        """Constructor"""
        canvas.Canvas.__init__(self, *args, **kwargs)
        just_face_regular = pdfmetrics.EmbeddedType1Face(afmFileRegular, pfbFileRegular)
        just_face_italic = pdfmetrics.EmbeddedType1Face(afmFileItalic, pfbFileItalic)
        face_name_regular = 'HPSimplifiedW02-Regular'
        face_name_italic = 'HPSimplifiedW02-Italic'
        pdfmetrics.registerTypeFace(just_face_regular)
        pdfmetrics.registerTypeFace(just_face_italic)
        just_font_regular = pdfmetrics.Font('HPSimplifiedW02-Regular', face_name_regular, 'WinAnsiEncoding')
        just_font_italic = pdfmetrics.Font('HPSimplifiedW02-Italic', face_name_italic, 'WinAnsiEncoding')
        pdfmetrics.registerFont(just_font_regular)
        pdfmetrics.registerFont(just_font_italic)
        self.pages = []

    # ----------------------------------------------------------------------
    def showPage(self):
        """
        On a page break, add information to the list
        """
        self.pages.append(dict(self.__dict__))
        self._startPage()

    # ----------------------------------------------------------------------

    def save(self):
        """
        Add the page number to each page (page x of y)
        """
        page_count = len(self.pages)
        for page in self.pages:
            self.__dict__.update(page)
            self.draw_page_number(page_count)
            canvas.Canvas.showPage(self)

        canvas.Canvas.save(self)

        # ----------------------------------------------------------------------

    def draw_page_number(self, page_count):
        """
        Add the page number
        """
        height = 9 * mm
        self.setStrokeColor(HexColor("#000000"))
        self.line(height, 15 * mm, 205 * mm, 15 * mm)
        page = "Faqe {} nga {}".format(self.getPageNumber(), page_count)
        self.setFont("HPSimplifiedW02-Regular", 10)
        self.drawRightString(205 * mm, height, page)
        # self.setFont("HPSimplifiedW02-Italic", 9)
        # self.drawString(height, 15.5 * mm, FACEBOOK_PAGE)
        # self.drawRightString(205 * mm, 15.5 * mm, INSTAGRAM_PAGE)
        self.setFont("HPSimplifiedW02-Regular", 9)
        self.drawRightString(205 * mm, 5 * mm, 'Mnudësuar nga,  ITWorks  http://www.it-works.io')
        self.setFont("HPSimplifiedW02-Regular", 10)
        self.drawString(height, height, "{}".format(timezone.now().strftime(DATE_FORMAT)))
