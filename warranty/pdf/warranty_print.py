import logging
import os

from reportlab.graphics.shapes import Drawing, Line
from reportlab.lib import colors
from reportlab.lib import utils
from reportlab.lib.enums import TA_CENTER, TA_RIGHT, TA_LEFT
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import cm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus import Image
from reportlab.platypus import KeepTogether
from reportlab.platypus import PageBreak
from reportlab.platypus import Paragraph
from reportlab.platypus import Table
from reportlab.platypus import TableStyle, Spacer, SimpleDocTemplate

from service_manager_backend.settings import DATE_FORMAT
from warranty.pdf.custom_canvas import CustomCanvas
from warranty.pdf.float_to_end import FloatToEnd

__author__ = 'StavroCons'

logger = logging.getLogger(__name__)
dir_path = os.path.dirname(os.path.realpath(__file__))
font_path = os.path.join(dir_path, 'hp_simplified')


class WarrantyPage:
    canvas = None

    pdfmetrics.registerFont(TTFont("Hp-Simplified", os.path.join(font_path, 'HP-SIMPLIFIED-REGULAR.ttf')))
    pdfmetrics.registerFont(TTFont("Hp-Simplified-Bold", os.path.join(font_path, 'HP-SIMPLIFIED-BOLD.ttf')))
    font_family = 'Hp-Simplified'
    font_family_bold = 'Hp-Simplified-Bold'
    font_normal_leading = 12
    font_small_size = 11
    spacer_width = 1 * cm
    spacer_height = 0.25 * cm
    spacer = KeepTogether(Spacer(spacer_width, spacer_height))
    middle_spacer = KeepTogether(Spacer(spacer_width, 0.50 * cm))
    large_spacer = KeepTogether(Spacer(spacer_width, 1 * cm))
    styles = getSampleStyleSheet()
    title_style = ParagraphStyle(name='sc-title', parent=styles['Normal'], fontName=font_family_bold, fontSize=15,
                                 leading=14.4, alignment=TA_CENTER, wordWrap='LTR')
    normal_style_right = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontName=font_family,
                                        fontSize=font_small_size, leading=font_normal_leading, alignment=TA_RIGHT,
                                        wordWrap='LTR', spaceAfter=5)
    last_normal_style_right = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontName=font_family,
                                             fontSize=font_small_size, leading=20, alignment=TA_RIGHT, wordWrap='LTR',
                                             spaceAfter=5)
    normal_style_left_bold = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontName=font_family_bold,
                                            fontSize=font_small_size, leading=font_normal_leading, alignment=TA_LEFT,
                                            wordWrap='LTR')
    normal_style_right_bold = ParagraphStyle(name='sc-normal', parent=styles['Normal'], fontName=font_family_bold,
                                             fontSize=font_small_size, leading=font_normal_leading, alignment=TA_RIGHT,
                                             wordWrap='LTR')
    centered_small_style = ParagraphStyle(name='sc-small', leftIndent=5, parent=styles['Normal'], fontName=font_family,
                                          fontSize=10, leading=font_normal_leading, alignment=TA_CENTER, wordWrap='LTR')
    left_small_style = ParagraphStyle(name='sc-small', leftIndent=5, parent=styles['Normal'], fontName=font_family,
                                      fontSize=10, leading=font_normal_leading, alignment=TA_LEFT, wordWrap='LTR')

    table_common_style = TableStyle([
        ('FONTNAME', (0, 0), (-1, 0), font_family_bold),
        ('FONTNAME', (0, 1), (-1, -1), font_family),
        ('FONTSIZE', (0, 1), (-3, -3), font_small_size),
        ('LEADING', (0, 1), (-1, -1), font_normal_leading),
        ('LEFTPADDING', (0, 0), (-1, -1), 0),
        ('RIGHTPADDING', (0, 0), (-1, -1), 0),
        ('LINEABOVE', (0, 1), (-1, 1), 0.5, colors.gray),
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
        ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
        ('GRID', (0, 0), (-1, -1), 0.5, colors.black),
        ('BACKGROUND', (0, 0), (-1, 0), colors.Color(0.8, 0.8, 0.8)),
        ('BACKGROUND', (0, 1), (-1, -1), colors.white)
    ])
    top_page_margin = 0.5 * cm
    bottom_page_margin = 2 * cm
    left_page_margin = right_page_margin = 0.5 * cm
    column_width = 120
    signature_line_width = 175

    def __init__(self, data, company):
        self.data = data
        self.company = company
        self.warranty_unit_table_info = [
            ['Produkti', 'IMEI i pajisjes', 'IMEI i sistemit operativ', 'Gjendja', 'Periudha']]
        self.prepare_warranty_units(data.warranty_units.all())

    def get_phones(self, data):
        phones = []
        for phone in data:
            phones.append(Paragraph(phone, style=self.normal_style_right))
        return phones

    def prepare_warranty_units(self, warranty_units):
        for wu in warranty_units:
            append_content = [
                Paragraph(str(wu.product), style=self.left_small_style),
                Paragraph(str(wu.imei_hardware), style=self.centered_small_style),
                Paragraph(str(wu.imei_software), style=self.centered_small_style),
                Paragraph('I ri' if wu.product_state == 1 else 'I përdorur', style=self.centered_small_style),
                Paragraph('{} {}'.format(str(wu.period), wu.measuring_unit.name), style=self.centered_small_style),
            ]
            self.warranty_unit_table_info.append(append_content)

    def print_page(self, filename):
        simple_doc_template = SimpleDocTemplate(filename, title='', pagesize=A4, topMargin=self.top_page_margin,
                                                rightMargin=self.right_page_margin,
                                                bottomMargin=self.bottom_page_margin, leftMargin=self.left_page_margin)
        flowables = self.header()
        flowables += self.body()
        simple_doc_template.build(flowables, canvasmaker=CustomCanvas)

    @staticmethod
    def footer(temp_canvas, _):
        temp_canvas.setFont("Helvetica", 9)

    def header(self):
        header_table_style = TableStyle([
            ("LINEBELOW", (0, 0), (-1, -1), 1, colors.black),
            ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
            ('LEADING', (0, -1), (-1, -1), 20),
        ])
        img = utils.ImageReader(self.company.get_logo_absolute_path())
        iw, ih = img.getSize()
        aspect = iw / float(ih)
        logo = Image(self.company.get_logo_absolute_path(), height=60, width=60 * aspect,
                     hAlign='LEFT')
        # logo = Image(self.company.get_logo_absolute_path(), width=120, height=55.5,
        #              hAlign='LEFT')
        address = Paragraph(self.company.address, style=self.normal_style_right)
        phone_numbers = self.get_phones(self.company.phone.split('|'))
        email = Paragraph(self.company.email, style=self.normal_style_right)
        nuis = Paragraph('NIPT-i {}'.format(self.company.nuis), style=self.last_normal_style_right)
        header_table = [
            [logo, [address, phone_numbers, email, nuis]],
        ]
        header_table_data = Table(header_table, colWidths='*', style=header_table_style)
        return [header_table_data]

    def body(self):
        title = Paragraph('<b>Fletë-garancie</b>', style=self.title_style)
        customer = Paragraph(str(self.data.customer.get_fullname()), style=self.normal_style_left_bold)
        address = Paragraph(str(self.data.customer.address if self.data.customer.address else ''),
                            style=self.normal_style_left_bold)
        phone = Paragraph(self.data.customer.phone, style=self.normal_style_left_bold)
        start_date = Paragraph(self.data.start_date.strftime(DATE_FORMAT), style=self.normal_style_right_bold)
        code = Paragraph(str(self.data.get_code()), style=self.normal_style_right_bold)

        warranty_table_info = [
            ["Klienti:", customer, Paragraph('Data e fillimit:', style=self.normal_style_right), start_date],
            ["Adresa:", address, Paragraph('Kodi:', style=self.normal_style_right), code],
            ["Tel:", phone]
        ]
        warranty_table = Table(warranty_table_info, colWidths=(2 * cm, None, None, 2.5 * cm),
                               style=TableStyle([('LEFTPADDING', (0, 0), (-1, -1), 0)]))
        warranty_unit_table = Table(self.warranty_unit_table_info, colWidths='*', style=self.table_common_style)

        atom_signature = Drawing(20, 20)
        atom_signature.add(Line(50, -5, 50 + self.signature_line_width, -5))

        customer_signature = Drawing(20, 20)
        customer_signature.add(Line(60, -5, 60 + self.signature_line_width, -5))

        signature = [
            [Paragraph('Jam dakord me shërbimin!', style=self.normal_style_left_bold)],
            [Paragraph(str(self.data.customer.get_fullname()), style=self.normal_style_left_bold)],
            [customer_signature]
            # [Paragraph("Manager", style=self.centered_small_style), Paragraph(str(self.data.customer.get_fullname()), style=self.centered_small_style)],
            # [atom_signature, customer_signature]
        ]
        signature_table = Table(signature, colWidths='*', style=TableStyle([('LEFTPADDING', (0, 0), (-1, -1), 0)]))

        return [self.middle_spacer, title, self.spacer, self.spacer, warranty_table, self.spacer, self.spacer,
                self.spacer, warranty_unit_table, FloatToEnd([signature_table]), PageBreak()]
