import base64

from django.http import HttpResponse
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from company.models import Company
from warranty.models import Warranty
from warranty.pdf.warranty_print import WarrantyPage


@api_view(['GET'])
def print_warranty(request, warranty_id):
    warranty = Warranty.objects.get(id=warranty_id)
    company = Company.objects.first()
    warranty_page = WarrantyPage(warranty, company)
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'inline; filename="{}.pdf"'.format(warranty.get_code())
    warranty_page.print_page(response)
    return Response({'data': base64.b64encode(response.content).decode(), 'title': warranty.get_code()},
                    status=status.HTTP_200_OK)
