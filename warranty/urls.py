from django.urls import path
from warranty.model_views.warranty_views import WarrantyListCreateAPIView, WarrantyRetrieveUpdateDestroyAPIView
from warranty.pdf.pdf_views import print_warranty
from warranty.views import get_initial_data_list

urlpatterns = [
    path('warranty/', WarrantyListCreateAPIView.as_view(), name='warranties'),
    path('warranty/<int:pk>/', WarrantyRetrieveUpdateDestroyAPIView.as_view(), name='warranty'),
    path('warranty/initial-data-list/', get_initial_data_list, name='initial-data-list'),

    path('print/warranty/<int:warranty_id>/', print_warranty, name='print-warranty'),

]
