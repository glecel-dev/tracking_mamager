from django.db import models

from common.models import Counter
from customer.models import Customer
from service_manager_backend.settings import DATE_TIME_FORMAT
from staff.models import User
from staff.models import Shop


class SCModel(models.Model):
    class Meta:
        app_label = 'archive'
        abstract = True


class Warranty(SCModel):
    class Meta:
        verbose_name = 'Garanci'
        verbose_name_plural = 'Garancitë'
        db_table = 'sc_warranty'
        ordering = ['-code']

    code = models.PositiveIntegerField('Kodi', default=0)
    customer = models.ForeignKey(Customer, verbose_name='customer', related_name='warranties', on_delete=models.CASCADE)
    creator = models.ForeignKey(User, verbose_name='creator', related_name='warranties', on_delete=models.CASCADE)
    date_created = models.DateTimeField('Data e krijimit', auto_now_add=True)
    date_last_updated = models.DateTimeField('Data e fundit e përitësimit', auto_now=True)
    start_date = models.DateTimeField('Data e fillimit')
    shop = models.ForeignKey(Shop, verbose_name='Dyqani', related_name='warranties', on_delete=models.CASCADE)
    copies = models.PositiveIntegerField("Kopje", default=0)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.get_code()

    def save(self, *args, **kwargs):
        if self.code == 0:
            self.code = Counter.next(self.shop.prefix_code)
        super(Warranty, self).save(*args, **kwargs)

    def get_code(self):
        return '{}-{}'.format(self.shop.prefix_code, self.code)

    def get_start_date_str(self):
        return self.start_date.strftime(DATE_TIME_FORMAT)


class MeasuringUnit(SCModel):
    class Meta:
        verbose_name = 'Measuring Unit'
        verbose_name_plural = 'Measuring Units'
        db_table = 'sc_measuring_unit'
        ordering = ['name']

    name = models.CharField('Njësia matëse', max_length=30)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return str(self.name)


PRODUCT_STATE = {
    (1, 'I ri'),
    (2, 'I përdorur')
    }


class WarrantyUnit(SCModel):
    class Meta:
        verbose_name = 'Warranty Unit'
        verbose_name_plural = 'Warranty Units'
        db_table = 'sc_warranty_unit'

    warranty = models.ForeignKey(Warranty, verbose_name='warranty', related_name='warranty_units',
                                 on_delete=models.CASCADE)
    product = models.CharField('Produkti', max_length=100)
    imei_hardware = models.CharField('IMEI i pajisjes', max_length=30)
    imei_software = models.CharField('IMEI i sistemit operativ', max_length=30)
    product_state = models.PositiveIntegerField('I ri', choices=PRODUCT_STATE, default=1)
    period = models.PositiveIntegerField('Periudha', default=6)
    measuring_unit = models.ForeignKey(MeasuringUnit, related_name='warranty_units', on_delete=models.PROTECT)

    def __str__(self):
        return str(self.product)
