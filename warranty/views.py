# Create your views here.
from rest_framework.decorators import api_view
from rest_framework.response import Response

from customer.models import Customer
from staff.models import Shop
from warranty.models import MeasuringUnit


@api_view(['GET'])
def get_initial_data_list(request):
    data = {
        'customer_list': [{'id': item.id, 'name': item.__str__()} for item in Customer.objects.filter(deleted=False)],
        'shop_list': [{'id': item.id, 'name': item.name} for item in Shop.objects.all()],
        'measuring_unit_list': [{'id': item.id, 'name': item.name} for item in MeasuringUnit.objects.all()]

        }
    return Response({'data': data})
