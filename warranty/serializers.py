from rest_framework import serializers
from rest_framework.fields import CharField, IntegerField, DateField
from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import Serializer

from warranty.models import MeasuringUnit, WarrantyUnit
from warranty.models import Warranty


# Warranty Unit Serializers
class WarrantyUnitReadSerializer(ModelSerializer):
    measuring_unit_name = serializers.ReadOnlyField(source='measuring_unit.name')

    class Meta:
        model = WarrantyUnit
        fields = ['product', 'imei_hardware', 'imei_software', 'product_state', 'period', 'measuring_unit',
                  'measuring_unit_name']


class WarrantyUnitWriteSerializer(ModelSerializer):
    class Meta:
        model = WarrantyUnit
        fields = ['product', 'imei_hardware', 'imei_software', 'product_state', 'period', 'measuring_unit']


# Warranty Serializers

class WarrantyListSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    code = serializers.ReadOnlyField(source='get_code')
    customer_fullname = serializers.SerializerMethodField(read_only=True)
    customer_phone = serializers.ReadOnlyField(source='customer.phone')
    creator = serializers.StringRelatedField(read_only=True)
    start_date = serializers.DateTimeField(read_only=True, format="%d/%m/%Y")

    def get_customer_fullname(self, obj):
        return obj.customer.get_fullname() if obj.customer else ''


class WarrantyReadSerializer(ModelSerializer):
    code = serializers.ReadOnlyField(source='get_code')
    customer_fullname = serializers.SerializerMethodField()
    customer_phone = serializers.ReadOnlyField(source='customer.phone')
    creator = serializers.StringRelatedField()
    warranty_units = WarrantyUnitReadSerializer(many=True)

    class Meta:
        model = Warranty
        fields = [
            'id', 'code', 'customer_fullname', 'customer', 'shop',
            'customer_phone', 'creator', 'start_date', 'date_created', 'warranty_units'
        ]

    def get_customer_fullname(self, obj):
        return obj.customer.get_fullname() if obj.customer else ''


class WarrantyWriteSerializer(ModelSerializer):
    warranty_units = WarrantyUnitWriteSerializer(many=True)

    class Meta:
        model = Warranty
        fields = ['id', 'customer', 'shop', 'start_date', 'warranty_units']

    def create(self, validated_data):
        validated_data['creator'] = self.context['request'].user
        warranty_units = validated_data.pop('warranty_units')
        warranty = super(WarrantyWriteSerializer, self).create(validated_data)
        for warranty_unit in warranty_units:
            WarrantyUnit.objects.create(**warranty_unit, warranty=warranty)
        return warranty

    def update(self, instance, validated_data):
        validated_data['creator'] = self.context['request'].user
        warranty_units = validated_data.pop('warranty_units')
        instance.warranty_units.all().delete()
        for warranty_unit in warranty_units:
            WarrantyUnit.objects.create(**warranty_unit, warranty=instance)
        return super(WarrantyWriteSerializer, self).update(instance, validated_data)


class WarrantyFilterSerializer(Serializer):
    code = CharField(max_length=50, required=False, allow_blank=True, allow_null=True)
    customer = IntegerField(required=False, allow_null=True)
    customer__phone = CharField(max_length=50, required=False, allow_blank=True, allow_null=True)
    customer_fullname = CharField(max_length=50, required=False, allow_blank=True, allow_null=True)
    shop = IntegerField(required=False, allow_null=True)
    # start_date = CharField(max_length=50, required=False, allow_blank=True, allow_null=True)

    start_date_min = serializers.DateField(required=False, allow_null=True)
    start_date_max = serializers.DateField(required=False, allow_null=True)
