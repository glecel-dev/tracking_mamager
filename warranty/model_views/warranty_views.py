from service_manager_backend.common.api_views import SMListCreateAPIView, SMRetrieveUpdateDestroyAPIView
from warranty.models import Warranty
from warranty.serializers import WarrantyReadSerializer, WarrantyWriteSerializer, WarrantyFilterSerializer, \
    WarrantyListSerializer


class WarrantyListCreateAPIView(SMListCreateAPIView):
    queryset = Warranty.objects.filter(deleted=False)
    read_serializer_class = WarrantyReadSerializer
    write_serializer_class = WarrantyWriteSerializer
    list_read_serializer_class = WarrantyListSerializer
    filter_serializer_class = WarrantyFilterSerializer
    filter_map = {}


class WarrantyRetrieveUpdateDestroyAPIView(SMRetrieveUpdateDestroyAPIView):
    queryset = Warranty.objects.filter(deleted=False)
    read_serializer_class = WarrantyReadSerializer
    write_serializer_class = WarrantyWriteSerializer
