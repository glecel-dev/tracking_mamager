from django.contrib import admin

# Register your models here.
from warranty.models import MeasuringUnit, Warranty, WarrantyUnit

admin.site.register(Warranty)
admin.site.register(MeasuringUnit)
admin.site.register(WarrantyUnit)
