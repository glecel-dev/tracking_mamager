from django.urls import reverse
from faker import Faker
from model_bakery import baker
from rest_framework import status
from rest_framework.test import APITestCase

from customer.models import Customer
from staff.models import Shop
from staff.tests.test_utils import generate_user_token
from warranty.models import MeasuringUnit, Warranty


class TestWarrantyListCreateAPIView(APITestCase):

    def setUp(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer {}'.format(generate_user_token()))
        self.url = reverse('warranties')
        self.customer = baker.make(Customer)
        self.shop = baker.make(Shop)
        self.measuring_unit = baker.make(MeasuringUnit)
        self.faker = Faker()

    def test_warranty_list_create(self):
        """
        Client requests to create an warranty!
        :return status code returns 201 CREATED!
        Client requests to get the warranty
        :return status code 200 OK!
        """
        warranty_data = {
            "customer": self.customer.id,
            "shop": self.shop.id,
            "start_date": "2020-01-28 00:00:00",
            "warranty_units": [
                {"product": "produ",
                 "imei_hardware": "a",
                 "imei_software": "u",
                 "product_state": 1,
                 "period": 6,
                 "measuring_unit": self.measuring_unit.id
                 }
            ]
        }
        response = self.client.post(self.url, warranty_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.get(self.url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class TestWarrantyUpdateDelete(APITestCase):
    def setUp(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer {}'.format(generate_user_token()))
        self.url = reverse('warranties')
        self.customer = baker.make(Customer)
        self.shop = baker.make(Shop)
        self.measuring_unit = baker.make(MeasuringUnit)

        self.faker = Faker()
        warranty_data = {
            "customer": self.customer.id,
            "shop": self.shop.id,
            "start_date": "2020-01-28 00:00:00",
            "warranty_units": [
                {"product": "produ",
                 "imei_hardware": "a",
                 "imei_software": "u",
                 "product_state": 1,
                 "period": 6,
                 "measuring_unit": self.measuring_unit.id
                 }
            ]
        }
        self.client.post(self.url, warranty_data, format='json')

    def test_update_warranty(self):
        """
        Client request update warranty!
        :return status 200 OK!
        """
        warranty = baker.make(Warranty)
        url = reverse('warranty', args=[warranty.id])
        customer = baker.make(Customer)
        shop = baker.make(Shop)
        measuring_unit = baker.make(MeasuringUnit)

        warranty_data = {
            "customer": customer.id,
            "shop": shop.id,
            "start_date": "2019-05-28 00:00:00",
            "warranty_units": [
                {"product": "test",
                 "imei_hardware": "test",
                 "imei_software": "test",
                 "product_state": 1,
                 "period": 6,
                 "measuring_unit": measuring_unit.id
                 }
            ]
        }

        response = self.client.put(url, warranty_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_warranty = Warranty.objects.get(pk=warranty.id)
        self.assertEqual(updated_warranty.customer, customer)
        self.assertEqual(updated_warranty.shop, shop)
