from django.test import TestCase
from faker import Faker
from model_bakery import baker
from customer.models import Customer
from warranty.models import Warranty, MeasuringUnit, WarrantyUnit


class TestWarranty(TestCase):
    def setUp(self):
        self.warranty = baker.make(Warranty)
        self.fake = Faker()

    def test_company_create(self):
        self.assertTrue(isinstance(self.warranty, Warranty))
        self.warranty.save()
        self.assertNotEqual(self.warranty.code, None)
        self.assertTrue(self.warranty.id is not None)
        self.assertEqual(self.warranty, Warranty.objects.get(pk=self.warranty.id))

    def test_company_update(self):
        old_customer = self.warranty.customer
        self.warranty.customer = baker.make(Customer)
        self.warranty.save()
        self.assertNotEqual(old_customer, self.warranty.customer)

    def test_company_delete(self):
        self.warranty.delete()
        self.assertFalse(Warranty.objects.filter(pk=self.warranty.id).exists())


class TestMeasuringUnit(TestCase):
    def setUp(self):
        self.unit = baker.make(MeasuringUnit)
        self.fake = Faker()

    def test_company_create(self):
        self.assertTrue(isinstance(self.unit, MeasuringUnit))
        self.unit.save()
        self.assertNotEqual(self.unit.name, None)
        self.assertTrue(self.unit.id is not None)
        self.assertEqual(self.unit, MeasuringUnit.objects.get(pk=self.unit.id))

    def test_company_update(self):
        old_name = self.unit.name
        self.unit.name = self.fake.name()
        self.unit.save()
        self.assertNotEqual(old_name, self.unit.name)

    def test_company_delete(self):
        self.unit.delete()
        self.assertFalse(MeasuringUnit.objects.filter(pk=self.unit.id).exists())


class TestWarrantyUnit(TestCase):
    def setUp(self):
        self.unit = baker.make(WarrantyUnit)
        self.fake = Faker()

    def test_company_create(self):
        self.assertTrue(isinstance(self.unit, WarrantyUnit))
        self.unit.save()
        self.assertNotEqual(self.unit.product, None)
        self.assertTrue(self.unit.id is not None)
        self.assertEqual(self.unit, WarrantyUnit.objects.get(pk=self.unit.id))

    def test_company_update(self):
        old_product = self.unit.product
        self.unit.product = self.fake.name()
        self.unit.save()
        self.assertNotEqual(old_product, self.unit.product)

    def test_company_delete(self):
        self.unit.delete()
        self.assertFalse(WarrantyUnit.objects.filter(pk=self.unit.id).exists())
